//
// Created by kekor on 03.12.18.
//

#ifndef SBOR_DEFINES_H
#define SBOR_DEFINES_H


namespace Actors {
    enum class Unit {
        Hero = 0,
        NPC,
        PistolBullet,
        Centaur,
        NPC_friend
    };

    enum class Items {
        Door = 0
    };

    enum class Action {
        Walk = 0,
        Shoot,
        Fight,
        ShootUsi,
        ShootAutomat
    };

    enum class Dirrection {
        Up = 0,
        Right = 1,
        Down = 2,
        Left = 4
    };
}

namespace Weapon {
    enum class Gun {
        Hand = 0,
        Pistol,
        Automat,
        Usi
    };
}

#endif //SBOR_DEFINES_H
