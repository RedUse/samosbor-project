#include "MusicPlayer.h"
#include "AllStates.h"
#include "unistd.h"

using std::make_unique;
using std::move;
using std::string;
using std::cout;
using std::endl;
using STATE_PTR = std::unique_ptr<State>;

MenuState::MenuState(StateManager *stack, GameContext &context_) :  State(stack, context_) {
    menuTexture1.loadFromFile("../GameGraphics/button.png");
    helpTexture.loadFromFile("../GameGraphics/help.png");
    menuBackground.loadFromFile("../GameGraphics/menu.jpg");
    menu1.setTexture(menuTexture1);
    menu2.setTexture(menuTexture1);
    menu3.setTexture(menuTexture1);
    help.setTexture(helpTexture);
    menuBg.setTexture(menuBackground);
    context.music.play(Music::MenuTheme); // Включаем музыку в меню
}

//  =========================================

bool MenuState::update_game(sf::Time /*deltaTime*/) {
    context.View_.reset(sf::FloatRect(0, 0, 1280, 720));
    return true;
}

//  =========================================

bool MenuState::draw_game() {

    menu1.setPosition(100, 100);
    menu2.setPosition(100, 200);
    menu3.setPosition(100, 300);
    menuBg.setPosition(0, 0);

    sf::Text game;
    sf::Font font;
    font.loadFromFile("../GameGraphics/font.ttf");
    game.setFont(font);
    game.setString(std::wstring(L"СТАРТ"));
    game.setCharacterSize(35);
    game.setColor(sf::Color::White);

    game.setPosition(210, 125);

    sf::Text help_btn;
    help_btn.setFont(font);
    help_btn.setString(std::wstring(L"ПОМОЩЬ"));
    help_btn.setCharacterSize(35);
    help_btn.setColor(sf::Color::White);

    help_btn.setPosition(195, 225);


    sf::Text exit;
    exit.setFont(font);
    exit.setString(std::wstring(L"ВЫХОД"));
    exit.setCharacterSize(35);
    exit.setColor(sf::Color::White);

    exit.setPosition(210, 325);


    menu1.setColor(sf::Color::White);
    menu2.setColor(sf::Color::White);
    menu3.setColor(sf::Color::White);
    menuNum = 0;
    context.mWindow->clear(sf::Color(1, 1, 0));
    context.mWindow->setView(context.View_);

    if (menu1.getGlobalBounds().contains(sf::Vector2f(sf::Mouse::getPosition(*context.mWindow).x, sf::Mouse::getPosition(*context.mWindow).y))) {
        menu1.setColor(sf::Color(164, 64, 132));
        menuNum = 1;
    }
    if (menu2.getGlobalBounds().contains(sf::Vector2f(sf::Mouse::getPosition(*context.mWindow).x, sf::Mouse::getPosition(*context.mWindow).y))) {
        menu2.setColor(sf::Color(164, 64, 132));
        menuNum = 2;
    }
    if ((menu3.getGlobalBounds().contains(sf::Vector2f(sf::Mouse::getPosition(*context.mWindow).x, sf::Mouse::getPosition(*context.mWindow).y)))) {
        menu3.setColor(sf::Color(164, 64, 132));
        menuNum = 3;
    }

    if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
        if (menuNum == 1) {handle_game_input(sf::Keyboard::G, true); /*context.music.play(Music::   MissionTheme);*/}                                    // hello kostil'
        if (menuNum == 2) {handle_game_input(sf::Keyboard::H, true); context.music.play(Music::RoflTheme);}
        if (menuNum == 3) {context.mWindow->close(); }
    }

    context.mWindow->draw(menuBg);
    context.mWindow->draw(menu1);
    context.mWindow->draw(menu2);
    context.mWindow->draw(menu3);

    context.mWindow->draw(game);
    context.mWindow->draw(help_btn);
    context.mWindow->draw(exit);

    context.mWindow->display();
}

//  =========================================

bool MenuState::handle_game_input(sf::Keyboard::Key key, bool isPressed) {
    if ((key == sf::Keyboard::G)&&(isPressed)) {
        pop_game_state();
        context.music.setPaused(1);
        context.music.play(Music::MissionTheme);
        if (stack->get_states_size() == 0) {
            STATE_PTR new_state = make_unique<IntroState>(stack, context);
            push_game_state(move(new_state));
        }
    }

    if ((key == sf::Keyboard::H)&&(isPressed)) {
        pop_game_state();
        context.music.play(Music::RoflTheme);
        STATE_PTR new_state = make_unique<HelpState>(stack, context);
        push_game_state(move(new_state));
    }
}