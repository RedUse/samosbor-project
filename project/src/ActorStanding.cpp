//
// Created by kekor on 02.12.18.
//
// Тупа говнокод
#include "AllActors.h"
#include "AllActorStates.h"

#define ATTACK_DISTANCE     70
#define PI 3.14159265
#define BAD_DIRRECTION -1
#define AMMO_FALSE     -2


using std::make_unique;
using std::move;
using ACTOR_PTR = std::unique_ptr<Actor>;

ActorStanding::ActorStanding(Actor *actor, ActorParams &actorParams_) : ActorState(actor, actorParams_) {
    Sprites_.resize(actorParams_.actionsAmount);
    for (int action = 0; action < actorParams_.actionsAmount; action++) {
        Sprites_[action].resize(actorParams_.dirrectionsAmount);

        for (int dirr = 0; dirr < actorParams_.dirrectionsAmount; dirr++) {
            Sprites_[action][dirr].resize(actorParams_.maxViews);
        }
    }


    for (int action = 0; action < actorParams_.actionsAmount; action++) {
        for (int dirr = 0; dirr < actorParams_.dirrectionsAmount; dirr++) {
            for (int view = 0; view < actorParams_.actionsViewsAmount[action]; view++) {
                sf::Sprite sp;
                sp.setTexture(actorParams_.actorTexture[action]);
                sp.setTextureRect(sf::IntRect(view * actorParams_.actionsWidth[action], dirr * actorParams_.actionsHeight[action],
                        actorParams_.actionsWidth[action], actorParams_.actionsHeight[action]));
                sp.setPosition(actorParams_.position.x, actorParams_.position.y);
                Sprites_[action][dirr][view] = sp;
            }
        }
    }
    buttonState = false;
    mouseState = false;
    mPlayer_ = Sprites_[(int)Actors::Action::Walk][(int)Actors::Dirrection::Left][0];
    view_ = 0;
    up_variant = int(Actors::Dirrection::Up);
    down_variant = int(Actors::Dirrection::Down);
};

//  ==================================================================================

bool ActorStanding::handle_actor_state_input(sf::Keyboard::Key key, bool isPressed) {

    buttonState = isPressed;

    if (key == sf::Keyboard::Z && !isPressed) {
        actorParams_.Zpressed = true;
    }
    if (key == sf::Keyboard::F && !isPressed) {
        actorParams_.Fpressed = true;
    }
    if (key == sf::Keyboard::X && !isPressed) {
        actorParams_.Xpressed = true;
    }
    if (key == sf::Keyboard::C) {
        actorParams_.Cpressed = true;
    }

    if (key == sf::Keyboard::W) {
        actorParams_.mIsMovingUp = isPressed;
        actorParams_.spriteUp = true;
    }
    else if (key == sf::Keyboard::S) {
        actorParams_.mIsMovingDown = isPressed;
        actorParams_.spriteDown = true;
    }
    else if (key == sf::Keyboard::A) {
        actorParams_.mIsMovingLeft = isPressed;
        actorParams_.spriteLeft = true;
    }
    else if (key == sf::Keyboard::D) {
        actorParams_.mIsMovingRight = isPressed;
        actorParams_.spriteRight = true;
    } else if (key == sf::Keyboard::Num1) {
        actorParams_.current_weapon = (int)Weapon::Gun::Hand;
        actorParams_.spriteRight = true;
    } else if (key == sf::Keyboard::Num2) {
        actorParams_.current_weapon = (int)Weapon::Gun::Pistol;
    } else if (key == sf::Keyboard::Num3) {
        actorParams_.current_weapon = (int)Weapon::Gun::Automat;
    } else if (key == sf::Keyboard::Num4) {
        actorParams_.current_weapon = (int)Weapon::Gun::Usi;
    }
    return true;
}

//  =========================================

bool ActorStanding::handle_actor_state_input(sf::Mouse::Button mouse, bool isPressed) {
    if (mouse == sf::Mouse::Left) {
        mouseState = isPressed;
    }
}

//  ==================================================================================

bool ActorStanding::update_actor_state(sf::Time deltaTime) {
    std::cout << actorParams_.Fpressed << std::endl;
    std::cout << int (actorParams_.x ) << ";" << int(actorParams_.y ) << std::endl;
    change_weapon();
    if (!check_attac(mouseState)) {
        return false;
    }


    //if (buttonState) {
        change_view(actorParams_, view_);
    //}
    move_actor(deltaTime);

    actor_->checkCollisionWithMap(actorParams_.dx, 0);
    actor_->checkCollisionWithMap(0, actorParams_.dy);
    actor_->checkCollisionWithInteractive(actorParams_.dx, 0);
    actor_->checkCollisionWithInteractive(0, actorParams_.dy);
    actor_->checkCollisionWithActors(actorParams_.dx, 0);
    actor_->checkCollisionWithActors(0, actorParams_.dy);

    mPlayer_.setPosition(actorParams_.x, actorParams_.y);

    actorParams_.dx = 0;
    actorParams_.dy = 0;
}

//  =========================================

bool ActorStanding::draw_actor_state() {
    actorParams_.context.mWindow->draw(mPlayer_);
}

//  =========================================

void ActorStanding::change_view(ActorParams &actorParams_, int &view_) {
    actorParams_.timeSinceLastChangeViev += actorParams_.clock.restart();
    if (actorParams_.timeSinceLastChangeViev > actorParams_.timeViewChange) {
        if (++view_ == actorParams_.actionsViewsAmount[(int)Actors::Action::Walk]) {
            view_ = 0;
        }
        actorParams_.timeSinceLastChangeViev = sf::Time::Zero;
    }
}

//  =========================================

int ActorStanding::handle_attac(int weapon, std::string actor_name) {
    int dirrection = BAD_DIRRECTION;
    sf::Vector2i int_pos = sf::Mouse::getPosition(*actorParams_.context.mWindow);
    sf::Vector2f f_pos;
    f_pos.x = float(int_pos.x);
    f_pos.y = float(int_pos.y);


    sf::Vector2u size = actorParams_.context.mWindow->getSize();
    if (fabs(f_pos.y - size.y/2) > fabs(f_pos.x - size.x/2)) {
        if ((f_pos.y - size.y/2) <= 0) {
            if ((buttonState) && (!actorParams_.mIsMovingUp)) {
                return BAD_DIRRECTION;
            }

            if ((f_pos.x - size.x/2) > 0) {
                up_variant = 0;
            } else {
                up_variant = 5;
            }

            dirrection = up_variant;
        } else {
            if ((buttonState) && (!actorParams_.mIsMovingDown)) {
                return BAD_DIRRECTION;
            }

            if ((f_pos.x - size.x/2) > 0) {
                down_variant = 2;
            } else {
                down_variant = 3;
            }

            dirrection = down_variant;
        }
    } else {
        if ((f_pos.x - size.x/2) <= 0) {
            if ((buttonState) && (!actorParams_.mIsMovingLeft)) {
                return BAD_DIRRECTION;
            }
            dirrection = int(Actors::Dirrection::Left);
        } else {
            if ((buttonState) && (!actorParams_.mIsMovingRight)) {
                return BAD_DIRRECTION;
            }
            dirrection = int(Actors::Dirrection::Right);
        }
    }


    if ((weapon == (int)Weapon::Gun::Pistol) || (weapon == (int)Weapon::Gun::Automat) || (weapon == (int)Weapon::Gun::Usi)) {
            ACTOR_PTR bullet = make_unique<PistolBullet>(actorParams_.position, actor_->obj_, actor_->ActorsPushQueue_,
                    actor_->ActorsStack_, actorParams_.context.allUnitsTextures_.get_unit_texture(Actors::Unit::PistolBullet),
                    actorParams_.context, f_pos, actorParams_, actor_->Items);
            actor_->push_actor(move(bullet));
    } else if (weapon == (int)Weapon::Gun::Hand) {
        ACTOR_PTR bullet = make_unique<HandBullet>(actorParams_.position, actor_->obj_, actor_->ActorsPushQueue_,
                actor_->ActorsStack_, actorParams_.context.allUnitsTextures_.get_unit_texture(Actors::Unit::PistolBullet),
                actorParams_.context, f_pos, actorParams_, actor_->Items);
        actor_->push_actor(move(bullet));
    }

    return dirrection;
}

//  =========================================

bool ActorStanding::anime(int weapon, int dirr, bool is_fire) {
    static int last_weapon = 0;
    static int fire_view = 1;
    static int fight_view = 0;
    if (last_weapon != weapon) {
        last_weapon = weapon;
        fire_view = 1;
        fight_view = 0;
    }
    static bool wasPressed = false;
    if (!is_fire) {
        fire_view = 1;
        if (weapon == (int)Weapon::Gun::Hand) {
            if (fight_view < actorParams_.actionsViewsAmount[(int) Actors::Action::Fight]) {
                actorParams_.fireReactionTimer += actorParams_.fireClock.restart();
                if (actorParams_.fireReactionTimer >= actorParams_.fireReaction) {
                    mPlayer_ = Sprites_[(int) Actors::Action::Fight][dirr][fight_view++];       // Опасно!!!!!!!!!!!
                    mPlayer_.setPosition(actorParams_.x, actorParams_.y);
                    actorParams_.fireReactionTimer = sf::Time::Zero;                            //Никому не лезть сюда тут смэрть
                }
                return false;
            }
        }
        actorParams_.fireReactionTimer += actorParams_.fireClock.restart();
        if ((actorParams_.fireReactionTimer >= actorParams_.fireReaction) && (wasPressed)) {
            mPlayer_ = Sprites_[(int) Actors::Action::Walk][dirr][0];
            mPlayer_.setPosition(actorParams_.x, actorParams_.y);
            actorParams_.fireReactionTimer = sf::Time::Zero;
            wasPressed = false;
        }
        return true;
    }


    wasPressed = true;
    if (fire_view == actorParams_.actionsViewsAmount[(int)Actors::Action::Shoot]) {
        fire_view = 0;
    }
    if (fight_view == actorParams_.actionsViewsAmount[(int)Actors::Action::Fight]) {
        fight_view = 0;
    }

    actorParams_.fireReactionTimer += actorParams_.fireClock.restart();
    if (actorParams_.fireReactionTimer >= actorParams_.fireReaction) {
        if ((weapon == (int)Weapon::Gun::Pistol) || (weapon == (int)Weapon::Gun::Automat)) {
            mPlayer_ = Sprites_[(int) Actors::Action::Shoot][dirr][fire_view++];
        } else if (weapon == (int)Weapon::Gun::Usi) {
            mPlayer_ = Sprites_[(int) Actors::Action::ShootUsi][dirr][fire_view++];
        } else if (weapon == (int)Weapon::Gun::Hand) {
            mPlayer_ = Sprites_[(int) Actors::Action::Fight][dirr][fight_view++];
        }
        mPlayer_.setPosition(actorParams_.x, actorParams_.y);
        actorParams_.fireReactionTimer = sf::Time::Zero;

    }
    return true;
}

//  =========================================

void ActorStanding::move_actor(sf::Time deltaTime) {
    if (actorParams_.mIsMovingUp) {
        actorParams_.dx = 0;
        actorParams_.dy = -(actorParams_.speed);
        if (view_ <= actorParams_.actionsViewsAmount[(int)Actors::Action::Walk]/2 - 1) {
            up_variant = 5;
        } else {
            up_variant = 0;
        }
        mPlayer_ = Sprites_[(int)Actors::Action::Walk][up_variant][view_];
        mPlayer_.setPosition(actorParams_.x, actorParams_.y);
    }

    if (actorParams_.mIsMovingDown) {
        actorParams_.dx = 0;
        actorParams_.dy = actorParams_.speed;

        if (view_ <= actorParams_.actionsViewsAmount[(int)Actors::Action::Walk]/2 - 1) {
            down_variant = 3;
        } else {
            down_variant = 2;
        }

        mPlayer_ = Sprites_[(int)Actors::Action::Walk][down_variant][view_];
        mPlayer_.setPosition(actorParams_.x, actorParams_.y);
    }

    if (actorParams_.mIsMovingLeft) {
        actorParams_.dx = -(actorParams_.speed);
        actorParams_.dy = 0;

        mPlayer_ = Sprites_[(int)Actors::Action::Walk][int(Actors::Dirrection::Left)][view_];
        mPlayer_.setPosition(actorParams_.x, actorParams_.y);
    }

    if (actorParams_.mIsMovingRight) {
        actorParams_.dx = actorParams_.speed;
        actorParams_.dy = 0;

        mPlayer_ = Sprites_[(int)Actors::Action::Walk][int(Actors::Dirrection::Right)][view_];
        mPlayer_.setPosition(actorParams_.x, actorParams_.y);
    }

    actorParams_.x = actorParams_.x + (actorParams_.dx * deltaTime.asSeconds());
    actorParams_.y = actorParams_.y + (actorParams_.dy * deltaTime.asSeconds());
    actorParams_.phisic_x = actorParams_.x + 9;
    actorParams_.phisic_y = actorParams_.y + 6;
    actorParams_.position.x = actorParams_.x;
    actorParams_.position.y = actorParams_.y;
}

//  =========================================

bool ActorStanding::check_attac(bool mouseState) {
    static int dirrection = BAD_DIRRECTION;
    if (mouseState) {
        if (actor_->weapon[actorParams_.current_weapon]->fire()) {
            dirrection = handle_attac(actor_->weapon[actorParams_.current_weapon]->get_name(), actorParams_.name);
        }
        if ((dirrection != BAD_DIRRECTION) && (actor_->weapon[actorParams_.current_weapon]->get_ammo())) { //Атака
            anime(actor_->weapon[actorParams_.current_weapon]->get_name(), dirrection, actor_->weapon[actorParams_.current_weapon]->check_coolDown());
        }
    } else {
        if (dirrection != BAD_DIRRECTION) {     //Переход в исходное и задержка при ударе
            return (anime(actor_->weapon[actorParams_.current_weapon]->get_name(), dirrection, mouseState));
        }
    }
    return true;
}

//  =========================================

void ActorStanding::change_weapon() {
    if (actor_->weapon[actorParams_.current_weapon]->get_ammo() <= 0) {
        for (auto weap = actor_->weapon.rbegin(); weap != actor_->weapon.rend(); weap++) {     // обходим стэк с вершины
            if ((*weap)->get_ammo() > 0) {
                actorParams_.current_weapon = (*weap)->get_name();
                return;
            }
        }
    }
}

















