//
// Created by den on 16.12.18.
//

#include "AllActors.h"
#include "AllActorStates.h"

using std::make_unique;
using std::move;
using std::string;
using std::cin;
using ACTOR_STATE_PTR = std::unique_ptr<ActorState>;

//  =========================================

Centaur::Centaur(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
std::vector<ACTOR_PTR> &ActorsStack, std::vector<sf::Texture> &texture, GameContext &context_,
        std::vector<ITEMS_PTR> &Items)

: Actor(position, obj, ActorsPushQueue, ActorsStack, texture, context_, Items) {

    actorParams_.actionsAmount = 3;
    actorParams_.actionsWidth.resize(actorParams_.actionsAmount);
    actorParams_.actionsHeight.resize(actorParams_.actionsAmount);
    actorParams_.actionsViewsAmount.resize(actorParams_.actionsAmount);

    actorParams_.actionsViewsAmount[(int)Actors::Action::Walk] = 8;         // Количество видов ходьбы
    actorParams_.actionsViewsAmount[(int)Actors::Action::Shoot] = 8;
    actorParams_.actionsViewsAmount[(int)Actors::Action::Fight] = 8;//!!!
    actorParams_.actionsWidth[(int)Actors::Action::Walk] = 125;              // Ширина спрайта ходьбы
    actorParams_.actionsWidth[(int)Actors::Action::Shoot] = 142;
    actorParams_.actionsWidth[(int)Actors::Action::Fight] = 142;//!!!
    actorParams_.actionsHeight[(int)Actors::Action::Walk] = 89;             // Высота спрайта ходьбы
    actorParams_.actionsHeight[(int)Actors::Action::Shoot] = 94;
    actorParams_.actionsHeight[(int)Actors::Action::Fight] = 94;//!!!
    actorParams_.maxViews = 8;
    actorParams_.dirrectionsAmount = 6;
    actorParams_.width = 125;
    actorParams_.height = 89;
    actorParams_.health = 1000;
    actorParams_.dx = 0;
    actorParams_.dy = 0;
    actorParams_.x = position.x;
    actorParams_.y = position.y;
    actorParams_.name = "enemy";
    actorParams_.mIsMovingUp = false;
    actorParams_.mIsMovingDown = false;
    actorParams_.mIsMovingLeft = false;
    actorParams_.mIsMovingRight = false;
    actorParams_.NPCAttac = false;
    actorParams_.speed = 150;
    actorParams_.sideRange = 80;
    actorParams_.Range = 80;
    actorParams_.rightRange = 160;
    actorParams_.leftRange = 60;
    actorParams_.upRange = 80;
    actorParams_.downRange = 100;
    actorParams_.maxHealth = actorParams_.health;
    actorParams_.maxSpeed = actorParams_.speed;

    actorParams_.timeViewChange = sf::seconds(1.f / 8.f);
    actorParams_.timeSinceLastChangeViev = actorParams_.timeViewChange;
    actorParams_.changePathTimer = sf::seconds(1.f / 8.f);
    actorParams_.changePath = actorParams_.changePathTimer;


    ACTOR_STATE_PTR first_state = make_unique<NPCStanding>(this, actorParams_);
    push_actor_state(move(first_state));

    WEAPON hand = make_unique<Hand>(1);
    WEAPON pistol = make_unique<Pistol>(0);

    float frequency = hand->get_temp() * 5;
    actorParams_.fireReaction = sf::seconds(1.f / frequency);                     // Частота смены выстрела
    actorParams_.fireReactionTimer = actorParams_.fireReaction;

    actorParams_.current_weapon = (int)Weapon::Gun::Hand;
    push_actor_gun(move(hand));
    push_actor_gun(move(pistol));

    init_graph();
    update_graph();
};




bool Centaur::update_actor(sf::Time deltaTime) {
    for (auto actorState = ActorsStatesStack_.rbegin(); actorState != ActorsStatesStack_.rend(); actorState++) {
        (*actorState)->update_actor_state(deltaTime);
    }
}


bool Centaur::draw_actor() {
    for (auto actorState = ActorsStatesStack_.rbegin();
         actorState != ActorsStatesStack_.rend(); actorState++) {
        if (actorParams_.health > 0) {
            (*actorState)->draw_actor_state();
        } else {
            sf::Sprite smert;
            smert.setTexture(actorParams_.actorTexture[3]);
            smert.setTextureRect(sf::IntRect(0, 0, 123, 60));
            smert.setPosition(actorParams_.x, actorParams_.y + 20);
            actorParams_.context.mWindow->draw(smert);
        }
    }
}


bool Centaur::checkCollisionWithMap(float Dx, float Dy) {
    for (int i = 0; i < obj_.size(); i++)
        if (getRect().intersects(obj_[i].rect))
        {
            if (obj_[i].type == "block") {
                if (Dy > 0) {
                    actorParams_.y = obj_[i].rect.top - actorParams_.height;
                    actorParams_.dy = 0;
                    return true;
                }
                if (Dy < 0) {
                    actorParams_.y = obj_[i].rect.top + obj_[i].rect.height;
                    actorParams_.dy = 0;
                    return true;
                }
                if (Dx > 0) {
                    actorParams_.x = obj_[i].rect.left - actorParams_.width;
                    return true;
                }
                if (Dx < 0) {
                    actorParams_.x = obj_[i].rect.left + obj_[i].rect.width;
                    return true;
                }
            }
        }
}

//  =========================================

bool Centaur::take_damage(int damage) {
    actorParams_.health -= damage;
    float max = actorParams_.maxHealth;
    float curr = actorParams_.health;
    float speed = actorParams_.maxSpeed;
    if (actorParams_.speed > 40) {
        actorParams_.speed = speed * (curr / max);
    }
}

sf::FloatRect Centaur::getRect() {
    return sf::FloatRect(actorParams_.x, actorParams_.y, actorParams_.width, actorParams_.height);       //эта ф-ция нужна для проверки столкновений
}

//  =========================================

bool Centaur::checkCollisionWithActors(float Dx, float Dy) {
    for (auto actor = ActorsStack_.begin(); actor != ActorsStack_.end(); actor++) {     // обходим стэк
        sf::FloatRect target = (*actor)->getRect();
        sf::FloatRect me = this->getRect();
        if ((target.intersects(me))  && (me != target) && ((*actor)->get_params().health > 0)) {
            if (Dy>0)    { actorParams_.y = target.top - actorParams_.height; }
            if (Dy<0)    { actorParams_.y = target.top + target.height; }
            if (Dx>0)    { actorParams_.x = target.left - actorParams_.width; }
            if (Dx<0)    { actorParams_.x = target.left + target.width; }
        }
    }
}

//  =========================================

bool Centaur::checkCollisionWithInteractive(float Dx, float Dy) {
    sf::FloatRect ggRect;
    for (int i = 0; i < Items.size(); i++)                               //проходимся по объектам
        if ((ggRect = getRect()).intersects(Items[i]->getRect()))                          //проверяем пересечение игрока с объектом
        {
            if (!Items[i]->check_open()) {
                if (Dy>0)    { actorParams_.y = Items[i]->getRect().top - actorParams_.height;  actorParams_.dy = 0; }      //onGround = true; }
                if (Dy<0)    { actorParams_.y = Items[i]->getRect().top + Items[i]->getRect().height;   actorParams_.dy = 0; }
                if (Dx>0)    { actorParams_.x = Items[i]->getRect().left - actorParams_.width; }
                if (Dx<0)    { actorParams_.x = Items[i]->getRect().left + Items[i]->getRect().width; }
            }
        }
}