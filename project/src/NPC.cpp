//
// Created by kekor on 02.12.18.
//
#include "AllActors.h"
#include "AllActorStates.h"

using std::make_unique;
using std::move;
using std::string;
using std::cin;
using ACTOR_STATE_PTR = std::unique_ptr<ActorState>;

//  =========================================

NPC::NPC(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
         std::vector<ACTOR_PTR> &ActorsStack, std::vector<sf::Texture> &texture, GameContext &context_,
         std::vector<ITEMS_PTR> &Items)

        : Actor(position, obj, ActorsPushQueue, ActorsStack, texture, context_, Items) {

    actorParams_.actionsAmount = 5;                                         // Количество действий
    actorParams_.actionsWidth.resize(actorParams_.actionsAmount);
    actorParams_.actionsHeight.resize(actorParams_.actionsAmount);
    actorParams_.actionsViewsAmount.resize(actorParams_.actionsAmount);

    actorParams_.actionsViewsAmount[(int)Actors::Action::Walk] = 8;         // Количество видов ходьбы
    actorParams_.actionsViewsAmount[(int)Actors::Action::Shoot] = 5;        // Количество видов выстрела
    actorParams_.actionsViewsAmount[(int)Actors::Action::Fight] = 8;        // Количество видов выстрела
    actorParams_.actionsViewsAmount[(int)Actors::Action::ShootUsi] = 5;        // Количество видов выстрела
    actorParams_.actionsViewsAmount[(int)Actors::Action::ShootAutomat] = 4;
    actorParams_.actionsWidth[(int)Actors::Action::Walk] = 44;              // Ширина спрайта ходьбы
    actorParams_.actionsWidth[(int)Actors::Action::Shoot] = 64;             // Ширина спрайта выстрела
    actorParams_.actionsWidth[(int)Actors::Action::Fight] = 59;
    actorParams_.actionsWidth[(int)Actors::Action::ShootUsi] = 64;
    actorParams_.actionsWidth[(int)Actors::Action::ShootAutomat] = 50;
    actorParams_.actionsHeight[(int)Actors::Action::Walk] = 70;             // Высота спрайта ходьбы
    actorParams_.actionsHeight[(int)Actors::Action::Shoot] = 78;            // Ширина спрайта выстрела
    actorParams_.actionsHeight[(int)Actors::Action::Fight] = 71;            // Ширина спрайта выстрела
    actorParams_.actionsHeight[(int)Actors::Action::ShootUsi] = 74;            // Ширина спрайта выстрела
    actorParams_.actionsHeight[(int)Actors::Action::ShootAutomat] = 69;
    actorParams_.maxViews = 8;
    actorParams_.dirrectionsAmount = 6;
    actorParams_.width = 44;
    actorParams_.height = 70;
    actorParams_.health = 1000;
    actorParams_.dx = 0;
    actorParams_.dy = 0;
    actorParams_.x = position.x;
    actorParams_.y = position.y;
    actorParams_.name = "npc";
    actorParams_.mIsMovingUp = false;
    actorParams_.mIsMovingDown = false;
    actorParams_.mIsMovingLeft = false;
    actorParams_.mIsMovingRight = false;
    actorParams_.NPCAttac = false;
    actorParams_.speed = 175;
    actorParams_.firstPosition.x  = actorParams_.x;
    actorParams_.firstPosition.y = actorParams_.y;
    actorParams_.sideRange = 90;
    actorParams_.Range = 90;
    actorParams_.maxHealth = actorParams_.health;
    actorParams_.maxHealth = actorParams_.health;
    actorParams_.maxSpeed = actorParams_.speed;

    actorParams_.timeViewChange = sf::seconds(1.f / 8.f);
    actorParams_.timeSinceLastChangeViev = actorParams_.timeViewChange;
    actorParams_.changePathTimer = sf::seconds(1.f / 8.f);
    actorParams_.changePath = actorParams_.changePathTimer;

    ACTOR_STATE_PTR first_state = make_unique<HelpNpsStanding>(this, actorParams_);
    push_actor_state(move(first_state));

    WEAPON hand = make_unique<Hand>(1);
    WEAPON pistol = make_unique<Pistol>(0);
    WEAPON automat = make_unique<Automat>(40);
    WEAPON usi = make_unique<Usi>(0);

    float frequency = hand->get_temp() * 5;
    actorParams_.fireReaction = sf::seconds(1.f / frequency);                     // Частота смены выстрела
    actorParams_.fireReactionTimer = actorParams_.fireReaction;

    actorParams_.current_weapon = (int)Weapon::Gun::Automat;
    push_actor_gun(move(hand));
    push_actor_gun(move(pistol));
    push_actor_gun(move(automat));
    push_actor_gun(move(usi));

    init_graph();
    update_graph();
};




bool NPC::update_actor(sf::Time deltaTime) {
    switch_safe();


    for (auto actorState = ActorsStatesStack_.rbegin(); actorState != ActorsStatesStack_.rend(); actorState++) {
        (*actorState)->update_actor_state(deltaTime);
    }
}


bool NPC::draw_actor() {
    for (auto actorState = ActorsStatesStack_.rbegin();
         actorState != ActorsStatesStack_.rend(); actorState++) {
        if (actorParams_.health > 0) {
            (*actorState)->draw_actor_state();
        } else {
            sf::Sprite smert;
            smert.setTexture(actorParams_.actorTexture[5]);
            smert.setTextureRect(sf::IntRect(0, 0, 76, 37));
            smert.setPosition(actorParams_.x, actorParams_.y + 20);
            actorParams_.context.mWindow->draw(smert);
        }
    }
}


bool NPC::checkCollisionWithMap(float Dx, float Dy) {
    for (int i = 0; i < obj_.size(); i++)
        if (getRect().intersects(obj_[i].rect))
        {
            if (obj_[i].type == "block") {
                if (Dy > 0) {
                    actorParams_.y = obj_[i].rect.top - actorParams_.height;
                    actorParams_.dy = 0;
                    return true;
                }
                if (Dy < 0) {
                    actorParams_.y = obj_[i].rect.top + obj_[i].rect.height;
                    actorParams_.dy = 0;
                    return true;
                }
                if (Dx > 0) {
                    actorParams_.x = obj_[i].rect.left - actorParams_.width;
                    return true;
                }
                if (Dx < 0) {
                    actorParams_.x = obj_[i].rect.left + obj_[i].rect.width;
                    return true;
                }
            }
        }
}

//  =========================================

bool NPC::take_damage(int damage) {
    actorParams_.health -= damage;
    actorParams_.health -= damage;
}

//  =========================================

sf::FloatRect NPC::getRect() {
    return sf::FloatRect(actorParams_.x, actorParams_.y, actorParams_.width, actorParams_.height);       //эта ф-ция нужна для проверки столкновений
}

//  =========================================

bool NPC::checkCollisionWithActors(float Dx, float Dy) {
    for (auto actor = ActorsStack_.begin(); actor != ActorsStack_.end(); actor++) {     // обходим стэк
        sf::FloatRect target = (*actor)->getRect();
        sf::FloatRect me = this->getRect();
        if ((target.intersects(me)) && (me != target) && ((*actor)->get_params().health > 0) && ((*actor)->get_params().name != "MainHero")) {
            if (Dy>0)    { actorParams_.y = target.top - actorParams_.height; }
            if (Dy<0)    { actorParams_.y = target.top + target.height; }
            if (Dx>0)    { actorParams_.x = target.left - actorParams_.width; }
            if (Dx<0)    { actorParams_.x = target.left + target.width; }
        }
    }
}

//  =========================================

bool NPC::checkCollisionWithInteractive(float Dx, float Dy) {
    sf::FloatRect ggRect;
    for (int i = 0; i < Items.size(); i++)                               //проходимся по объектам
        if ((ggRect = getRect()).intersects(Items[i]->getRect()))                          //проверяем пересечение игрока с объектом
        {
            if (!Items[i]->check_open()) {
                if (Dy>0)    { actorParams_.y = Items[i]->getRect().top - actorParams_.height;  actorParams_.dy = 0; }      //onGround = true; }
                if (Dy<0)    { actorParams_.y = Items[i]->getRect().top + Items[i]->getRect().height;   actorParams_.dy = 0; }
                if (Dx>0)    { actorParams_.x = Items[i]->getRect().left - actorParams_.width; }
                if (Dx<0)    { actorParams_.x = Items[i]->getRect().left + Items[i]->getRect().width; }
            }
        }
}


