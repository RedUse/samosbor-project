#include <iostream>
#include <SFML/Graphics.hpp>

/*sf::View View;
void setPlayerCoordinateForView(float x, float y) {
	float tempX = x; float tempY = y;

	//if (x < 320) tempX = 320;
	//if (y < 240) tempY = 240;//верхнюю сторону
	//if (y > 20) tempY = 20;//нижнюю стороню.для новой карты

	View.setCenter(tempX, tempY);
}*/

#include "GameEngine.h"
#include "StateManager.h"
#include "State.h"

#include "level.h"
#include "view.h"



using std::cout;
using std::cin;
using std::endl;
using std::string;

int main(int argc, char *argv[]) {
	GameEngine game;
	while (game.running()) {
	    game.run();
	}
}

/*
#include <SFML/Graphics.hpp>
#include <iostream>
#include <sstream>
#include "iostream"
#include "level.h"
#include <vector>
#include "view_.h"
 
 
using namespace sf;
////////////////////////////////////Общий класс-родитель//////////////////////////
class Entity {
public:
    std::vector<Object> obj;//вектор объектов карты
    float dx, dy, x, y, speed,moveTimer;
    int w,h,health;
    bool life, isMove, onGround;
    Texture texture;
    Sprite sprite;
    String name;
    Entity(Image &image, String Name, float X, float Y, int W, int H){
        x = X;
        y = Y;
        w = W;
        h = H;
        name = Name;
        moveTimer = 0;
        speed = 0; health = 100; dx = 0; dy = 0;
        life = true; onGround = false; isMove = false;
        texture.loadFromImage(image);
        sprite.setTexture(texture);
        sprite.setOrigin(w / 2, h / 2);
    }
 
    FloatRect getRect(){//ф-ция получения прямоугольника. его коорд,размеры (шир,высот).
        return FloatRect(x, y, w, h);//эта ф-ция нужна для проверки столкновений 
    }
};
////////////////////////////////////////////////////КЛАСС ИГРОКА////////////////////////
class Player :public Entity {
public:
    enum { left, right, up, down, stay } state;
    int playerScore;
 
    Player(Image &image, String Name, Level &lev, float X, float Y, int W, int H) :Entity(image, Name, X, Y, W, H ){
           playerScore = 0; state = stay; obj = lev.GetAllObjects();//инициализируем.получаем все объекты для взаимодействия персонажа с картой
           if (name == "Player1"){
               sprite.setTextureRect(IntRect(0, 0, w, h));
           }
       }
 
       void control(float time){
           if (Keyboard::isKeyPressed){
               float CurrentFrame = 0;
               if (Keyboard::isKeyPressed(Keyboard::Left)) {
                   state = left; speed = 0.1;
                   CurrentFrame += 0.005 * time;
                   if (CurrentFrame > 4) CurrentFrame -= 4;
                   sprite.setTextureRect(IntRect(22 * int(CurrentFrame), 66, w, h));
               }
               if (Keyboard::isKeyPressed(Keyboard::Right)) {
                   state = right; speed = 0.1;
                   CurrentFrame += 0.005 * time;
                   if (CurrentFrame > 4) CurrentFrame -= 4;
                   sprite.setTextureRect(IntRect(22 * int(CurrentFrame), 99, w, h));
               }
*/
              /* if ((Keyboard::isKeyPressed(Keyboard::Up)) && (onGround)) {
                   state = jump; dy = -0.6; onGround = false;
               }*/
/*


               if (Keyboard::isKeyPressed(Keyboard::Up)) {
                   state = up; speed = 0.1;
                   CurrentFrame += 0.005 * time;
                   if (CurrentFrame > 4) CurrentFrame -= 4;
                   sprite.setTextureRect(IntRect(22 * int(CurrentFrame), 33, w, h));
               }

               if (Keyboard::isKeyPressed(Keyboard::Down)) {
                   state = down; speed = 0.1;
                   CurrentFrame += 0.005 * time;
                   if (CurrentFrame > 4) CurrentFrame -= 4;
                   sprite.setTextureRect(IntRect(22 * int(CurrentFrame), 0, w, h));
               }
           }
       } 
 
       
 
       void checkCollisionWithMap(float Dx, float Dy)
       {
           for (int i = 0; i<obj.size(); i++)//проходимся по объектам
           if (getRect().intersects(obj[i].rect))//проверяем пересечение игрока с объектом
           {
               if (obj[i].name == "block")//если встретили препятствие
               {
                   if (Dy>0)    { y = obj[i].rect.top - h;  dy = 0; }//onGround = true; }
                   if (Dy<0)    { y = obj[i].rect.top + obj[i].rect.height;   dy = 0; }
                   if (Dx>0)    { x = obj[i].rect.left - w; }
                   if (Dx<0)    { x = obj[i].rect.left + obj[i].rect.width; }
               }
           }
       }
        
       void update(float time)
       {
           control(time);
           switch (state)
           {
           case right: dx = speed; dy = 0;   break;
           case left:  dx = -speed; dy = 0;  break;
           //case up: break;
           case up:    dx = 0; dy = -speed;  break;
           case down:  dx = 0; dy = speed;   break;
           case stay:  break;
           }
           x += dx*time;
           checkCollisionWithMap(dx, 0);
           y += dy*time;
           checkCollisionWithMap(0, dy);
           //sprite.setPosition(x + w / 2, y + h / 2);
           sprite.setPosition(x, y);
           if (health <= 0){ life = false; }
           if (!isMove){ speed = 0; }
           setPlayerCoordinateForView(x, y);
           if (life) { setPlayerCoordinateForView(x, y); }
           //dy = dy + 0.00015*time;
       } 
};
 
 
 
class Enemy :public Entity{
public:
    Enemy(Image &image, String Name,Level &lvl, float X, float Y, int W, int H) :Entity(image, Name, X, Y, W, H){
        obj = lvl.GetObjects("solid");//инициализируем.получаем нужные объекты для взаимодействия врага с картой
        if (name == "EasyEnemy"){
            sprite.setTextureRect(IntRect(0, 0, w, h));
            dx = 0.1;
        }
    }
 
    void checkCollisionWithMap(float Dx, float Dy)
    {
        for (int i = 0; i<obj.size(); i++)//проходимся по объектам
        if (getRect().intersects(obj[i].rect))//проверяем пересечение игрока с объектом
        {
            //if (obj[i].name == "solid"){//если встретили препятствие (объект с именем solid)
                if (Dy>0)   { y = obj[i].rect.top - h;  dy = 0; onGround = true; }
                if (Dy<0)   { y = obj[i].rect.top + obj[i].rect.height;   dy = 0; }
                if (Dx>0)   { x = obj[i].rect.left - w;  dx = -0.1; sprite.scale(-1, 1); }
                if (Dx<0)   { x = obj[i].rect.left + obj[i].rect.width; dx = 0.1; sprite.scale(-1, 1); }
            //}
        }
    }
 
    void update(float time)
    {
        if (name == "EasyEnemy"){
            //moveTimer += time;if (moveTimer>3000){ dx *= -1; moveTimer = 0; }//меняет направление примерно каждые 3 сек
            checkCollisionWithMap(dx, 0);
            x += dx*time;
            sprite.setPosition(x + w / 2, y + h / 2);
            if (health <= 0){ life = false; }
        }
    }
};



int main()
{
    RenderWindow window(VideoMode(640, 480), "SamoSbor");
    view.reset(FloatRect(0, 0, 640, 480));
 
    Level lvl;//создали экземпляр класса уровень
    lvl.LoadFromFile("../GameGraphics/mmm.tmx");//загрузили в него карту, внутри класса с помощью методов он ее обработает.
 
    Image heroImage;
    heroImage.loadFromFile("../GameGraphics/humanun2.png");
    //heroImage.createMaskFromColor(Color(255, 255, 255));
 
 //   Image easyEnemyImage;
 //   easyEnemyImage.loadFromFile("images/shamaich.png");
 //   easyEnemyImage.createMaskFromColor(Color(255, 0, 0));
 
    Object player=lvl.GetObject("player");//объект игрока на нашей карте.задаем координаты игроку в начале при помощи него
 //   Object easyEnemyObject = lvl.GetObject("easyEnemy");//объект легкого врага на нашей карте.задаем координаты игроку в начале при помощи него
 
    Player p(heroImage, "Player1", lvl, player.rect.left, player.rect.top, 22, 33);//передаем координаты прямоугольника player из карты в координаты нашего игрока
  //  Enemy easyEnemy(easyEnemyImage, "EasyEnemy", lvl, easyEnemyObject.rect.left, easyEnemyObject.rect.top, 200, 97);//передаем координаты прямоугольника easyEnemy из карты в координаты нашего врага
    
    Clock clock;
    while (window.isOpen())
    {
 
        float time = clock.getElapsedTime().asMicroseconds();
 
        clock.restart();
        time = time / 800;
        
        Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close(); 
        }       
        p.update(time);
//        easyEnemy.update(time);
 view.reset(FloatRect(0, 0, 640, 480));
        window.setView(view);
        window.clear();
        lvl.Draw(window);//рисуем новую карту
 
//        window.draw(easyEnemy.sprite);
        window.draw(p.sprite);
        window.display();
    }
    return 0;
}
 */

