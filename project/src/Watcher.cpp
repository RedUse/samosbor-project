//
// Created by anita on 12/15/18.
//
#define DIALOG_DISTANCE     80
#define USABLE_DISTANCE     100

#include "Watcher.h"
#include "level.h"
#include <string>
using ACTOR_PTR = std::unique_ptr<Actor>;

Watcher::Watcher(GameContext &context, std::vector<ACTOR_PTR> &ActorsStack, std::vector<ITEMS_PTR> &Items, std::vector<Object> &MapObjects, Level &lvl):
context(context),  ActorsStack_(ActorsStack), Items(Items), MapObjects(MapObjects), lvl(lvl) {


    samosborBegin = sf::Time::Zero;
    samosborBeginTimer = sf::seconds(10);

    samosborEnd = sf::Time::Zero;
    samosborEndTimer = sf::seconds(30);



    font.loadFromFile("../GameGraphics/font.ttf");

    hud.loadFromFile("../GameGraphics/hud.png");
    hud_bar.setTexture(hud);

    gg_hud_t.loadFromFile("../GameGraphics/gg.png");
    gg_hud.setTexture(gg_hud_t);

    com_face.loadFromFile("../GameGraphics/com_dialog.png");
    comand_face.setTexture(com_face);

    com_name.setFont(font);
    com_name.setString(std::wstring(L"ПОЛКОВНИК \n   ТИТАЕВ"));
    com_name.setCharacterSize(16);
    com_name.setColor(sf::Color::White);

    task_title.setFont(font);
    task_title.setString(std::wstring(L"БОЕВАЯ ЗАДАЧА:"));
    task_title.setStyle(sf::Text::Bold);
    task_title.setCharacterSize(23);
    task_title.setColor(sf::Color(125, 37, 22));

    pistol_t.loadFromFile("../GameGraphics/pistol.png");
    pistol.setTexture(pistol_t);

    uzi_t.loadFromFile("../GameGraphics/uzi.png");
    uzi.setTexture(uzi_t);

    automat_t.loadFromFile("../GameGraphics/automat.png");
    automat.setTexture(automat_t);
}



bool Watcher::check_interactive(sf::FloatRect Rect_gg) {
    for (auto door = Items.begin(); door != Items.end(); door++) {     // обходим стэк с вершины
        if ((*door)->get_name() == "closedDoor") {
            sf::FloatRect door_rect = (*door)->getRect();
            float current_distance = sqrtf(
                    powf(std::abs(door_rect.left - Rect_gg.left), 2) + powf(std::abs(door_rect.top - Rect_gg.top), 2));
            if (current_distance < USABLE_DISTANCE) {
                if ((*door)->check_open()) {
                    context.sound.play(SoundEffect::CloseDoor);
                    (*door)->close_door();
                } else {
                    context.sound.play(SoundEffect::OpenDoor);
                    (*door)->open_door();
                }
            }
        }
    }
}

//  =========================================

bool Watcher::SAMOSBOR_UPDATE() {
    if (!is_samosbor()) {
        for (int i = 0; i < MapObjects.size(); i++)
            if ((MapObjects[i].type == "hall") || (MapObjects[i].type == "unsafe")) {
                int layer = atoi(MapObjects[i].name.c_str());
                lvl.increase_opaciti(layer, -1);
            }
        return false;
    }

    for (int i = 0; i < MapObjects.size(); i++)
        if ((MapObjects[i].type == "hall") || (MapObjects[i].type == "unsafe")) {
            int layer = atoi(MapObjects[i].name.c_str());
            lvl.increase_opaciti(layer, 1);
        }
}

//  =========================================

bool Watcher::draw_timer() {
    sf::Text sbor;
    sbor.setFont(font);
    int sec = (int)samosborBeginTimer.asSeconds() - (int)samosborBegin.asSeconds();
    if (sec <= 0) {
        return true;
    };
    std::wstring t = L"     САМОСБОР\n   ЧЕРЕЗ ";
    t += std::to_wstring(sec);
    t += std::wstring(L" СЕК!");
    sbor.setString(t);
    sbor.setCharacterSize(21);
    sbor.setStyle(sf::Text::Bold);
    sbor.setColor(sf::Color(125, 37, 22));
    //sf::Vector2u size = context.mWindow->getSize();

    sbor.setOrigin(sbor.getLocalBounds().width/2, sbor.getLocalBounds().height/2 - 100);

//    for (int i = 0; i < ActorsStack_.size(); i++) {
//        if (ActorsStack_[i]->get_params().name == "MainHero") {
//            sbor.setPosition(ActorsStack_[i]->get_params().context.View_.getCenter());
//        }
//    }
 //   sbor.setPosition(context.View_.getCenter().x + 45 , context.View_.getCenter().y + 280);

    sbor.setPosition(context.View_.getCenter().x + 85, context.View_.getCenter().y + 210);
    context.mWindow->draw(sbor);
}

//  =========================================

int Watcher::SAMOSBOR_DAMAGE(int latNum) {
    return lvl.get_layer_damage(latNum)/5;
}

//  =========================================

bool Watcher::is_samosbor() {
    if (SAMOSBOR) {
        samosborBegin += samosborBeginClock.restart();
        samosborEnd += samosborEndClock.restart();
        if (samosborBegin > samosborEndTimer) {
            SAMOSBOR = false;
            return false;
        }

        if (samosborBegin > samosborBeginTimer) {
            return true;
        }
    }
    return false;
}

//  ==================================================================================

bool Watcher::check_first_dialog() {
    first_dialog_gg_comand = false;
    comand_dead = false;
    show_help = false;
    sf::FloatRect Rect_gg;
    sf::FloatRect Rect_npc_friend;
    for (int i = 0; i < ActorsStack_.size(); i++) {
        if (ActorsStack_[i]->get_params().name == "MainHero") {
            Rect_gg = ActorsStack_[i]->getRect();
            if (ActorsStack_[i]->actorParams_.Fpressed) {
                check_interactive(Rect_gg);
                if (!mission) {
                    first_dialog_gg_comand = true;
                }
                else {
                    if (mission_done){
                        third_dialog = true;
                        dialog3_ended = false;
                    }
                    else {
                        second_dialog = true;
                        dialog2_ended = false;
                    }
                }
                ActorsStack_[i]->actorParams_.can_walking = false;
            }
            if (ActorsStack_[i]->actorParams_.Zpressed && second_dialog) {
                dialog2_ended = true;
                ActorsStack_[i]->actorParams_.Fpressed = false;
                ActorsStack_[i]->actorParams_.can_walking = true;
                ActorsStack_[i]->actorParams_.Zpressed = false;

            }
            if (ActorsStack_[i]->actorParams_.Zpressed && third_dialog) {
                dialog3_ended = true;
                ActorsStack_[i]->actorParams_.Fpressed = false;
                ActorsStack_[i]->actorParams_.can_walking = true;
                ActorsStack_[i]->actorParams_.Zpressed = false;

            }
            if (ActorsStack_[i]->actorParams_.Zpressed && first_dialog_gg_comand && first_dialog_choice1_2) {
                first_dialog_choice1_end = true;
                ActorsStack_[i]->actorParams_.Fpressed = false;
                ActorsStack_[i]->actorParams_.can_walking = true;
                ActorsStack_[i]->actorParams_.Zpressed = false;

            }
            else if (ActorsStack_[i]->actorParams_.Zpressed && first_dialog_gg_comand && first_dialog_choice2) {
                first_dialog_choice2_end = true;
                ActorsStack_[i]->actorParams_.Fpressed = false;
                ActorsStack_[i]->actorParams_.can_walking = true;
                ActorsStack_[i]->actorParams_.Zpressed = false;
                ActorsStack_[i]->actorParams_.health = 0;

            }
            else if (ActorsStack_[i]->actorParams_.Zpressed && first_dialog_gg_comand && first_dialog_choice3) {
                first_dialog_choice3_end = true;
                ActorsStack_[i]->actorParams_.Fpressed = false;
                ActorsStack_[i]->actorParams_.can_walking = true;
                ActorsStack_[i]->actorParams_.Zpressed = false;
            }
            else if (ActorsStack_[i]->actorParams_.Zpressed && first_dialog_gg_comand && first_dialog_choice1_1) {
                first_dialog_choice1_2 = true;
                ActorsStack_[i]->actorParams_.Zpressed = false;

            }
            else if (ActorsStack_[i]->actorParams_.Zpressed && first_dialog_gg_comand && first_dialog_choice1) {
                first_dialog_choice1_1 = true;
                ActorsStack_[i]->actorParams_.Zpressed = false;

            }
            else  if (ActorsStack_[i]->actorParams_.Zpressed && first_dialog_gg_comand && !first_dialog_choice1 && !first_dialog_choice2 && !first_dialog_choice3) {
                first_dialog_choice1 = true;
                ActorsStack_[i]->actorParams_.Zpressed = false;
            }
            else if (ActorsStack_[i]->actorParams_.Xpressed && first_dialog_gg_comand && !first_dialog_choice1 && !first_dialog_choice2 && !first_dialog_choice3) {
                first_dialog_choice2 = true;
            }
            else if (ActorsStack_[i]->actorParams_.Cpressed && first_dialog_gg_comand && !first_dialog_choice1 && !first_dialog_choice2 && !first_dialog_choice3) {
                first_dialog_choice3 = true;
            }

        }
        if (ActorsStack_[i]->get_params().name == "npc_friend") {
            Rect_npc_friend = ActorsStack_[i]->getRect();
            if (ActorsStack_[i]->actorParams_.health <= 0) {
                comand_dead = true;
            }

        }
    }
    float current_distance = sqrtf(powf(std::abs(Rect_npc_friend.left - Rect_gg.left), 2) + powf(std::abs(Rect_npc_friend.top - Rect_gg.top), 2));
    if (current_distance < DIALOG_DISTANCE) {
        show_help = true;
    }
    else {
        show_help = false;
        for (int i = 0; i < ActorsStack_.size(); i++) {
            if (ActorsStack_[i]->get_params().name == "MainHero") {
                ActorsStack_[i]->actorParams_.Fpressed = false;
            }
        }
        first_dialog_gg_comand = false;
        second_dialog = false;
        third_dialog = false;

    }
    if (dialog3_ended) {
        if (!samosbor_restarted) {
            SAMOSBOR = true;
            samosborBeginClock.restart();
            samosborEndClock.restart();
            samosbor_restarted =true;
        }
    }
    if (dialog1_ended) {
        for (int i = 0; i < ActorsStack_.size(); i++) {
            if (ActorsStack_[i]->get_params().name == "npc") {
                ActorsStack_[i]->actorParams_.following = true;
            }
        }
    }

}

//  ==================================================================================

bool Watcher::write_help_dialog() {
    sf::Text help;
    help.setFont(font);
    help.setString(std::wstring(L"Press F to\n"
                                "Pay Respects"));
    help.setCharacterSize(22);
    help.setColor(sf::Color::White);
    help.setPosition(context.View_.getCenter().x + 45 , context.View_.getCenter().y + 280);
    context.mWindow->draw(help);
}

//  ==================================================================================

bool Watcher::write_dialog1_com_init() {
    sf::Text com1;
    com1.setFont(font);
    com1.setString(std::wstring(L"      У тебя наряд, щенок,\n"
                                "           где ты шляешься?"));
    com1.setCharacterSize(25);
    com1.setColor(sf::Color::White);
    com1.setPosition(context.View_.getCenter().x - 450 , context.View_.getCenter().y - 350);

    sf::Text choice1;
    choice1.setFont(font);
    choice1.setStyle(sf::Text::Italic);
    choice1.setString(std::wstring(L"Z: Извиняюсь, товарищ полковник, \n"
                                   "   отходил по нужде\n"
                                   "X: Веселился вместе с вашей дочкой\n"
                                   "C: Помогал доставлять раненого в\n"
                                   "   мед. блок"));
    choice1.setCharacterSize(25);
    choice1.setColor(sf::Color::White);
    choice1.setPosition(context.View_.getCenter().x - 450, context.View_.getCenter().y - 280);


    comand_face.setTexture(com_face);
    comand_face.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350 );

    sf::RectangleShape shape(sf::Vector2f(530, 220));;
    shape.setFillColor(sf::Color(0,0,0,180));

    shape.setOutlineThickness(2.5);
    shape.setOutlineColor(sf::Color::White);
    shape.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350);

    context.mWindow->draw(shape);
    context.mWindow->draw(comand_face);
    context.mWindow->draw(com1);
    context.mWindow->draw(choice1);

    com_name.setPosition(context.View_.getCenter().x - 570, context.View_.getCenter().y - 210);
    context.mWindow->draw(com_name);

}

//  ==================================================================================

bool Watcher::write_dialog1_com1() {
    sf::Text com1;
    com1.setFont(font);
    com1.setString(std::wstring(L"Недавно на службе и уже обосрался?\n"
                                "            *смеется*\n"
                                "Ладно, шутки в сторону, слушай сюда\n"
                                "Пришел приказ зачистить этаж 984\n"
                                "Ты знаешь, что это теперь наша зона?"));

    com1.setCharacterSize(25);
    com1.setColor(sf::Color::White);
    com1.setPosition(context.View_.getCenter().x - 450 , context.View_.getCenter().y - 350);

    sf::Text choice1;
    choice1.setFont(font);
    choice1.setStyle(sf::Text::Italic);
    choice1.setString(std::wstring(L"\nZ: А как же ребята из 27-ого?\n"));
    choice1.setCharacterSize(25);
    choice1.setColor(sf::Color::White);
    choice1.setPosition(context.View_.getCenter().x - 450, context.View_.getCenter().y - 200);


    comand_face.setTexture(com_face);
    comand_face.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350 );


    sf::RectangleShape shape(sf::Vector2f(530, 220));;
    shape.setFillColor(sf::Color(0,0,0,180));

    shape.setOutlineThickness(2.5);
    shape.setOutlineColor(sf::Color::White);
    shape.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350);


    context.mWindow->draw(shape);
    context.mWindow->draw(comand_face);
    context.mWindow->draw(com1);
    context.mWindow->draw(choice1);

    com_name.setPosition(context.View_.getCenter().x - 570, context.View_.getCenter().y - 210);
    context.mWindow->draw(com_name);

}

//  ==================================================================================

bool Watcher::write_dialog1_com1_1() {
    sf::Text com1;
    com1.setFont(font);
    com1.setString(std::wstring(L"Отставить вопросы.\n"));

    com1.setCharacterSize(25);
    com1.setColor(sf::Color::White);
    com1.setPosition(context.View_.getCenter().x - 450 , context.View_.getCenter().y - 350);

    sf::Text choice1;
    choice1.setFont(font);
    choice1.setStyle(sf::Text::Italic);
    choice1.setString(std::wstring(L"\nZ: Промолчать\n"));
    choice1.setCharacterSize(25);
    choice1.setColor(sf::Color::White);
    choice1.setPosition(context.View_.getCenter().x - 450, context.View_.getCenter().y - 200);

    comand_face.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350 );


    sf::RectangleShape shape(sf::Vector2f(530, 220));;
    shape.setFillColor(sf::Color(0,0,0,180));

    shape.setOutlineThickness(2.5);
    shape.setOutlineColor(sf::Color::White);
    shape.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350);


    context.mWindow->draw(shape);
    context.mWindow->draw(comand_face);
    context.mWindow->draw(com1);
    context.mWindow->draw(choice1);

    com_name.setPosition(context.View_.getCenter().x - 570, context.View_.getCenter().y - 210);
    context.mWindow->draw(com_name);

}

//  ==================================================================================

bool Watcher::write_dialog1_com1_2() {
    sf::Text com1;
    com1.setFont(font);
    com1.setString(std::wstring(L"Слушай приказ. Твой наряд закончен,\n"
                                "тебя сменит 38-ой. Ты отправляешься \n"
                                "на этаж 984 на зачистку. С тобой пойдут\n"
                                "рядовые Смирнов и Бережной.\n"
                                "Командуешь ты"));

    com1.setCharacterSize(25);
    com1.setColor(sf::Color::White);
    com1.setPosition(context.View_.getCenter().x - 450 , context.View_.getCenter().y - 350);

    sf::Text choice1;
    choice1.setFont(font);
    choice1.setStyle(sf::Text::Italic);
    choice1.setString(std::wstring(L"\nZ: Так точно, товарищ полковник\n"));
    choice1.setCharacterSize(25);
    choice1.setColor(sf::Color::White);
    choice1.setPosition(context.View_.getCenter().x - 450, context.View_.getCenter().y - 200);


    comand_face.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350 );


    sf::RectangleShape shape(sf::Vector2f(530, 220));;
    shape.setFillColor(sf::Color(0,0,0,180));

    shape.setOutlineThickness(2.5);
    shape.setOutlineColor(sf::Color::White);
    shape.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350);


    context.mWindow->draw(shape);
    context.mWindow->draw(comand_face);
    context.mWindow->draw(com1);
    context.mWindow->draw(choice1);

    com_name.setPosition(context.View_.getCenter().x - 570, context.View_.getCenter().y - 210);
    context.mWindow->draw(com_name);

}

//  ==================================================================================

bool Watcher::write_dialog1_com2() {
    sf::Text com1;
    com1.setFont(font);
    com1.setString(std::wstring(L"Так ты у нас шутник?\n"
                                "А как тебе такие шутки?\n"));
    com1.setCharacterSize(25);
    com1.setColor(sf::Color::White);
    com1.setPosition(context.View_.getCenter().x - 450 , context.View_.getCenter().y - 350);

    sf::Text choice1;
    choice1.setFont(font);
    choice1.setStyle(sf::Text::Italic);
    choice1.setString(std::wstring(L"Z: ..."));
    choice1.setCharacterSize(25);
    choice1.setColor(sf::Color::White);
    choice1.setPosition(context.View_.getCenter().x - 450, context.View_.getCenter().y - 200);


    comand_face.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350 );


    sf::RectangleShape shape(sf::Vector2f(530, 220));;
    shape.setFillColor(sf::Color(0,0,0,180));

    shape.setOutlineThickness(2.5);
    shape.setOutlineColor(sf::Color::White);
    shape.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350);

    context.mWindow->draw(shape);
    context.mWindow->draw(comand_face);
    context.mWindow->draw(com1);
    context.mWindow->draw(choice1);

    com_name.setPosition(context.View_.getCenter().x - 570, context.View_.getCenter().y - 210);
    context.mWindow->draw(com_name);

}

//  ==================================================================================

bool Watcher::write_dialog1_com3() {
    sf::Text com1;
    com1.setFont(font);
    com1.setString(std::wstring(L"Помощь товарищу - похвально, но ты на\n"
                                "посту. Так что тебе наряд вне очереди.\n"
                                "Слушай новый приказ. Ты отправляешься \n"
                                "на зачистку этажа 984. С тобой пойдут\n"
                                "рядовые Смирнов и Бережной.\n"
                                "Командуешь ты\n"));
    com1.setCharacterSize(25);
    com1.setColor(sf::Color::White);
    com1.setPosition(context.View_.getCenter().x - 450 , context.View_.getCenter().y - 350);

    sf::Text choice1;
    choice1.setFont(font);
    choice1.setStyle(sf::Text::Italic);
    choice1.setString(std::wstring(L"\nZ: Так точно товарищ полковник\n"));
    choice1.setCharacterSize(25);
    choice1.setColor(sf::Color::White);
    choice1.setPosition(context.View_.getCenter().x - 450, context.View_.getCenter().y - 190);



    comand_face.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350 );


    sf::RectangleShape shape(sf::Vector2f(530, 220));;
    shape.setFillColor(sf::Color(0,0,0,180));

    shape.setOutlineThickness(2.5);
    shape.setOutlineColor(sf::Color::White);
    shape.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350);


    context.mWindow->draw(shape);
    context.mWindow->draw(comand_face);
    context.mWindow->draw(com1);
    context.mWindow->draw(choice1);

    com_name.setPosition(context.View_.getCenter().x - 570, context.View_.getCenter().y - 210);
    context.mWindow->draw(com_name);

}

//  ==================================================================================

bool Watcher::draw_first_task() {

    sf::Text task;
    task.setFont(font);
    task.setString(std::wstring(L"Поговорить с командиром\n"));
    task.setCharacterSize(22);
    task.setColor(sf::Color::White);
    task.setPosition(context.View_.getCenter().x - 308, context.View_.getCenter().y + 280);

    task_title.setPosition(context.View_.getCenter().x - 300, context.View_.getCenter().y + 250);

    context.mWindow->draw(task_title);
    context.mWindow->draw(task);

}

//  ==================================================================================

bool Watcher::draw_second_task() {

    sf::Text task;
    task.setFont(font);
    task.setString(std::wstring(L"Очистить этаж 984\n"));
    task.setCharacterSize(22);
    task.setColor(sf::Color::White);
    task.setPosition(context.View_.getCenter().x - 300, context.View_.getCenter().y + 280);

    task_title.setPosition(context.View_.getCenter().x - 300, context.View_.getCenter().y + 250);

    context.mWindow->draw(task_title);
    context.mWindow->draw(task);

}


bool Watcher::draw_last_task() {

    sf::Text task;
    task.setFont(font);
    task.setString(std::wstring(L"Выжить от самосбора\n"));
    task.setCharacterSize(22);
    task.setColor(sf::Color::White);
    task.setPosition(context.View_.getCenter().x - 300, context.View_.getCenter().y + 280);

    task_title.setPosition(context.View_.getCenter().x - 300, context.View_.getCenter().y + 250);

    context.mWindow->draw(task_title);
    context.mWindow->draw(task);

}

//  ==================================================================================

bool Watcher::draw_hud() {

    hud_bar.setPosition(context.View_.getCenter().x - 434, context.View_.getCenter().y + 230 );

    gg_hud.setPosition(context.View_.getCenter().x - 55, context.View_.getCenter().y + 280 );

    context.mWindow->draw(hud_bar);
    context.mWindow->draw(gg_hud);
}

//  ==================================================================================

bool Watcher::draw_hp() {
    std::string hp;
    for (int i = 0; i < ActorsStack_.size(); i++) {
        if (ActorsStack_[i]->get_params().name == "MainHero") {
            hp = std::to_string(ActorsStack_[i]->actorParams_.health);
        }
    }

    sf::Text hp_text;
    hp_text.setFont(font);
    hp_text.setString("HP: " + hp);
    hp_text.setCharacterSize(20);
    hp_text.setColor(sf::Color::White);
    hp_text.setPosition(context.View_.getCenter().x + 82, context.View_.getCenter().y + 230);
    context.mWindow->draw(hp_text);
}

//  ==================================================================================

bool Watcher::draw_weapon() {

    for (int i = 0; i < ActorsStack_.size(); i++) {
        if (ActorsStack_[i]->get_params().name == "MainHero") {
            int gg_current_weapon = ActorsStack_[i]->get_params().current_weapon;

            // int max_magazine = ActorsStack_[i]->weapon[gg_current_weapon]->get_max_magazine();
            int current_ammo = ActorsStack_[i]->weapon[gg_current_weapon]->get_ammo();
            int current_magazine = ActorsStack_[i]->weapon[gg_current_weapon]->get_magazine();

            std::string current_amo_str;
            if (current_ammo == 0 && current_magazine == 0) {
                current_amo_str = std::to_string(current_ammo);

            }
            else {
                current_amo_str = std::to_string(current_ammo - current_magazine);

            }


            std::string current_magazine_str = std::to_string(current_magazine);

            sf::Text weapon_text;
            weapon_text.setFont(font);
            weapon_text.setString(current_magazine_str +"/" + current_amo_str);
            weapon_text.setCharacterSize(20);
            weapon_text.setColor(sf::Color::White);
            weapon_text.setPosition(context.View_.getCenter().x + 250, context.View_.getCenter().y + 230);

            if (gg_current_weapon == 1) {
                pistol.setPosition(context.View_.getCenter().x + 210, context.View_.getCenter().y + 282);
                context.mWindow->draw(pistol);
                context.mWindow->draw(weapon_text);

            }
            else if (gg_current_weapon== 3){
                uzi.setPosition(context.View_.getCenter().x + 210, context.View_.getCenter().y + 282);
                context.mWindow->draw(uzi);
                context.mWindow->draw(weapon_text);
            }

        }
    }

}


bool Watcher::write_dialog2_go_out() {
    sf::Text com1;
    com1.setFont(font);
    com1.setString(std::wstring(L"Какого хрена ты еще здесь!?\n"));

    com1.setCharacterSize(25);
    com1.setColor(sf::Color::White);
    com1.setPosition(context.View_.getCenter().x - 450 , context.View_.getCenter().y - 350);

    sf::Text choice1;
    choice1.setFont(font);
    choice1.setStyle(sf::Text::Italic);
    choice1.setString(std::wstring(L"\nZ: Промолчать\n"));
    choice1.setCharacterSize(25);
    choice1.setColor(sf::Color::White);
    choice1.setPosition(context.View_.getCenter().x - 450, context.View_.getCenter().y - 200);

    comand_face.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350 );


    sf::RectangleShape shape(sf::Vector2f(530, 220));;
    shape.setFillColor(sf::Color(0,0,0,180));

    shape.setOutlineThickness(2.5);
    shape.setOutlineColor(sf::Color::White);
    shape.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350);


    context.mWindow->draw(shape);
    context.mWindow->draw(comand_face);
    context.mWindow->draw(com1);
    context.mWindow->draw(choice1);

    com_name.setPosition(context.View_.getCenter().x - 570, context.View_.getCenter().y - 210);
    context.mWindow->draw(com_name);

}

bool Watcher::check_mission_done() {
    int dead_mobs = 0;
    for (int i = 0; i < ActorsStack_.size(); i++) {
        if (ActorsStack_[i]->get_params().name == "enemy" && ActorsStack_[i]->get_params().health <= 0) {
            dead_mobs++;
        }
    }
    if (dead_mobs == 2) {
        mission_done = true;
    }
}

bool Watcher::write_dialog3() {
    sf::Text com1;
    com1.setFont(font);
    com1.setString(std::wstring(L"Отлично, салага!\n"
                                "Ты справился с самым простым заданием\n"
                                "Так теперь... Стоп, ты чувствуешь запах?\n"
                                "Б**ть, самосбор начинается, \nчертову сирену так и не починили.\n"
                                "Бегом закрой гермодверь!"));

    com1.setCharacterSize(25);
    com1.setColor(sf::Color::White);
    com1.setPosition(context.View_.getCenter().x - 450 , context.View_.getCenter().y - 350);

    sf::Text choice1;
    choice1.setFont(font);
    choice1.setStyle(sf::Text::Italic);
    choice1.setString(std::wstring(L"\nZ: Промолчать\n"));
    choice1.setCharacterSize(25);
    choice1.setColor(sf::Color::White);
    choice1.setPosition(context.View_.getCenter().x - 450, context.View_.getCenter().y - 200);

    comand_face.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350 );


    sf::RectangleShape shape(sf::Vector2f(530, 220));;
    shape.setFillColor(sf::Color(0,0,0,180));

    shape.setOutlineThickness(2.5);
    shape.setOutlineColor(sf::Color::White);
    shape.setPosition(context.View_.getCenter().x - 600, context.View_.getCenter().y - 350);


    context.mWindow->draw(shape);
    context.mWindow->draw(comand_face);
    context.mWindow->draw(com1);
    context.mWindow->draw(choice1);

    com_name.setPosition(context.View_.getCenter().x - 570, context.View_.getCenter().y - 210);
    context.mWindow->draw(com_name);
}













