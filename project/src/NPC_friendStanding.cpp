//
// Created by kekor on 12.12.18.
//

#include "AllActors.h"
#include "AllActorStates.h"

#define PI 3.14159265
#define BAD_DIRRECTION -1

using std::make_unique;
using std::move;
using ACTOR_PTR = std::unique_ptr<Actor>;

NPC_friendStanding::NPC_friendStanding(Actor *actor, ActorParams &actorParams_) : ActorState(actor, actorParams_) {

    Sprites_.resize(actorParams_.actionsAmount);
    for (int action = 0; action < actorParams_.actionsAmount; action++) {
        Sprites_[action].resize(actorParams_.dirrectionsAmount);

        for (int dirr = 0; dirr < actorParams_.dirrectionsAmount; dirr++) {
            Sprites_[action][dirr].resize(actorParams_.maxViews);
        }
    }


    for (int action = 0; action < actorParams_.actionsAmount; action++) {
        for (int dirr = 0; dirr < actorParams_.dirrectionsAmount; dirr++) {
            for (int view = 0; view < actorParams_.actionsViewsAmount[action]; view++) {
                sf::Sprite sp;
                sp.setTexture(actorParams_.actorTexture[action]);
                sp.setTextureRect(sf::IntRect(view * actorParams_.actionsWidth[action], dirr * actorParams_.actionsHeight[action],
                                              actorParams_.actionsWidth[action], actorParams_.actionsHeight[action]));
                sp.setPosition(actorParams_.position.x, actorParams_.position.y);
                Sprites_[action][dirr][view] = sp;
            }
        }
    }
    buttonState = false;
    mouseState = false;
    mPlayer_ = Sprites_[(int)Actors::Action::Walk][(int)Actors::Dirrection::Right][0];
    view_ = 0;
    up_variant = int(Actors::Dirrection::Up);
    down_variant = int(Actors::Dirrection::Down);
};

//  =========================================

bool NPC_friendStanding::update_actor_state(sf::Time deltaTime) {
//    sf::FloatRect Rect;
//    buttonState = true;
//    for (int i = 0; i < actor_->ActorsStack_.size(); i++) {
//        if (actor_->ActorsStack_[i]->get_params().name == "MainHero") {      //   Тут подправил
//            Rect = actor_->ActorsStack_[i]->getRect();
//            float current_distance = sqrtf(powf(std::abs(actorParams_.x - Rect.left), 2) + powf(std::abs(actorParams_.y - Rect.top), 2));   //TO DO: Мерить от центра спрайта
//            if (current_distance < DIALOG_DISTANCE) {
//                std::cout << "Привет";
//            } else {
//                actorParams_.NPCAttac = false;
//            }
//        }
//    }
}

//  =========================================

bool NPC_friendStanding::draw_actor_state() {
    actorParams_.context.mWindow->draw(mPlayer_);
}
