//
// Created by kekor on 15.12.18.
//

#include "InteractiveObjects.h"

InteractiveObjects::InteractiveObjects(std::string name_, sf::Rect<float> rect_, sf::Texture &texture, GameContext &context_, int distr)
: name(name_), rect(rect_), itemTexture(texture), context(context_), district(distr) {
    state = false;
    broken = false;
    health = 10000;
}

//  =========================================

Door::Door(std::string name_, sf::Rect<float> rect_, sf::Texture &texture, GameContext &context_, int distr)
: InteractiveObjects(name_, rect_, texture, context_, distr) {
    view_amount = 2;
    rect.width = 48;
    rect.height = 60;
    spriteWidth = 48;
    spriteHeight = 96;
    state = false;
    broken = false;
    health = 500;

    item.resize(view_amount);
    for (size_t view = 0; view < view_amount; view++) {
        sf::Sprite sp;
        sp.setTexture(itemTexture);
        sp.setTextureRect(sf::IntRect(view * spriteWidth, 0, spriteWidth, spriteHeight));
        sp.setPosition(rect.left, rect.top);
        item[view] = sp;
    }
}

//  =========================================

bool InteractiveObjects::check_open() {
    if (health <= 0) {
        broken = true;
    }
    return (state | broken);
}

//  =========================================

sf::FloatRect Door::getRect() {
    return rect;
}

//  =========================================

bool Door::draw_items() {
    if (check_open()) {
        context.mWindow->draw(item[1]);
    } else {
        context.mWindow->draw(item[0]);
    }
}

