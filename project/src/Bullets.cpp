//
// Created by kekor on 09.12.18.
//

#include "AllActors.h"
#include "ActorState.h"
#include "AllActorStates.h"


#define PI 3.14159265

using std::make_unique;
using std::move;
using std::string;
using std::cin;
using ACTOR_STATE_PTR = std::unique_ptr<ActorState>;

//  =========================================

PistolBullet::PistolBullet(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
                           std::vector<ACTOR_PTR> &ActorsStack, std::vector<sf::Texture> &texture, GameContext &context_,
                           sf::Vector2f target, ActorParams &autor, std::vector<ITEMS_PTR> &Items)
        : Actor(position, obj, ActorsPushQueue, ActorsStack, texture, context_, Items), autor(autor) {

    actorParams_.actionsAmount = 1;
    actorParams_.actionsViewsAmount.resize(actorParams_.actionsAmount);
    actorParams_.actionsViewsAmount[(int)Actors::Action::Walk] = 1;
    actorParams_.maxViews = 1;
    actorParams_.dirrectionsAmount = 1;
    actorParams_.actionsAmount = 1;
    target_x = target.x;
    target_y = target.y;
    actorParams_.x = position.x + autor.width/2;
    actorParams_.y = position.y + autor.height/2;
    actorParams_.health = 200;
    actorParams_.width = 26;
    actorParams_.height = 4;
    actorParams_.speed = 1000;
    actorParams_.damage = 40;
    actorParams_.dx = 0;
    actorParams_.dy = 0;
    actorParams_.name = "bullet";
    actorParams_.mIsMovingUp = false;
    actorParams_.mIsMovingDown = false;
    actorParams_.mIsMovingLeft = false;
    actorParams_.mIsMovingRight = false;
    actorParams_.timeSinceLastChangeViev = sf::Time::Zero;


    if (autor.name == "MainHero") {
        set_dirrection();
    } else {
        npc_set_dirrection();
    }

    Sprites_.resize(1);
    for (int dirr = 0; dirr < 1; dirr++) {
        Sprites_[dirr].resize(1);
    }

    for (int dirr = 0; dirr < 1; dirr++) {
        for (int view = 0; view < 1; view++) {
            sf::Sprite sp;
            sp.setTexture(actorParams_.actorTexture[0]);
            sp.setTextureRect(sf::IntRect(26, 14, actorParams_.width, actorParams_.height));
            sp.setPosition(actorParams_.position.x, actorParams_.position.y);
            Sprites_[dirr][view] = sp;
        }
    }


    mPlayer_ = Sprites_[0][0];
    set_rotation();
};

//  =========================================

bool PistolBullet::update_actor(sf::Time deltaTime) {
    if (autor.name == "MainHero") {
        sf::Vector2u size = actorParams_.context.mWindow->getSize();
        angle = atan(fabs((target_x - size.x / 2) / (target_y - size.y / 2)));
    } else {
        sf::Vector2f bullet(actorParams_.x, actorParams_.y);
        angle = atan(fabs((target_x - bullet.x)/(target_y - bullet.y)));
    }
    set_rotation();

    if (actorParams_.mIsMovingUp) {
        actorParams_.dy = -(actorParams_.speed * cos(angle));
        end_y = (actorParams_.y - actorParams_.height/2) - actorParams_.width * cos(angle);
    }

    if (actorParams_.mIsMovingDown) {
        actorParams_.dy = actorParams_.speed * cos(angle);
        end_y = (actorParams_.y - actorParams_.height/2) + actorParams_.width * cos(angle);
    }

    if (actorParams_.mIsMovingLeft) {
        actorParams_.dx = -(actorParams_.speed * sin(angle));
        end_x = actorParams_.x - actorParams_.width * sin(angle);
    }

    if (actorParams_.mIsMovingRight) {
        actorParams_.dx = actorParams_.speed * sin(angle);
        end_x = actorParams_.x + actorParams_.width * sin(angle);
    }

    actorParams_.x = actorParams_.x + (actorParams_.dx * deltaTime.asSeconds());
    actorParams_.y = actorParams_.y + (actorParams_.dy * deltaTime.asSeconds());



    mPlayer_.setPosition(actorParams_.x, actorParams_.y);
    actorParams_.timeSinceLastChangeViev += actorParams_.clock.restart();

    actorParams_.dx = 0;
    actorParams_.dy = 0;

    checkCollisionWithMap(0, 0);
    checkCollisionWithActors(0, 0);
    checkCollisionWithInteractive(0, 0);

}

//  =========================================

bool PistolBullet::draw_actor() {
    actorParams_.context.mWindow->draw(mPlayer_);
}

//  =========================================

bool PistolBullet::checkCollisionWithMap(float Dx, float Dy) {
    sf::FloatRect endRect(end_x, end_y, 2, 2);
    for (int i = 0; i < obj_.size(); i++)
        if (endRect.intersects(obj_[i].rect))
        {
            if (obj_[i].name == "wall") {
                actorParams_.health = 0;
            }
        }
}

//  =========================================

bool PistolBullet::checkCollisionWithActors(float Dx, float Dy) {
    for (auto actor = ActorsStack_.begin(); actor != ActorsStack_.end(); actor++) {     // обходим стэк
        sf::FloatRect target = (*actor)->getRect();
        sf::FloatRect begin(autor.x, autor.y, autor.width, autor.height);
        if ((target.intersects(this->getRect())) && (begin != target) && (this->getRect() != target) && ((*actor)->get_params().health > 0)
        && ((*actor)->get_params().name != "bullet") && ((*actor)->get_params().name != autor.name)) {
            if (((*actor)->get_params().name != "MainHero") || (autor.name != "npc")) {
                (*actor)->take_damage(actorParams_.damage);
                actorParams_.health = 0;
            }
        }
    }
}

//  =========================================

float PistolBullet::set_rotation() {
    float rotation_angle = angle * 180/PI;
    if (actorParams_.mIsMovingLeft) {
        rotation_angle += 90;
    } else {
        rotation_angle = 90 - rotation_angle;
    }

    if (actorParams_.mIsMovingUp) {
        rotation_angle *= (-1);
    }
    mPlayer_.setRotation(rotation_angle);
}

//  =========================================

bool PistolBullet::set_dirrection() {
    sf::Vector2u size = actorParams_.context.mWindow->getSize();
    angle = atan(fabs((target_x - size.x/2)/(target_y - size.y/2)));
    if (int(target_x - size.x/2) >= 0) {
        actorParams_.mIsMovingRight = true;
    } else if (int(target_x - size.x/2) < 0) {
        actorParams_.mIsMovingLeft = true;
    }

    if (int(target_y - size.y/2) >= 0) {
        actorParams_.mIsMovingDown = true;
    } else if (int(target_y - size.y/2) < 0) {
        actorParams_.mIsMovingUp = true;
    }
}

//  =========================================

bool PistolBullet::npc_set_dirrection() {
    sf::Vector2f bullet(actorParams_.x, actorParams_.y);
    angle = atan(fabs((target_x - bullet.x) / (target_y - bullet.y)));
    if (int(target_x -bullet.x) >= 0) {
        actorParams_.mIsMovingRight = true;
    } else if (int(target_x -bullet.x) < 0) {
        actorParams_.mIsMovingLeft = true;
    }

    if (int(target_y -bullet.y) >= 0) {
        actorParams_.mIsMovingDown = true;
    } else if (int(target_y -bullet.y) < 0) {
        actorParams_.mIsMovingUp = true;
    }
}

//  =========================================

bool PistolBullet::take_damage(int damage) {
    actorParams_.health -= damage;
}

//  =========================================

bool PistolBullet::checkCollisionWithInteractive(float Dx, float Dy) {
    for (int i = 0; i < Items.size(); i++) {                            //проходимся по объектам
        sf::FloatRect target = Items[i]->getRect();
        sf::FloatRect end = this->getRect();
        if ( target.intersects(end) && (Items[i]->get_name() == "closedDoor")) {
            Items[i]->take_damage(actorParams_.damage);
            actorParams_.health = 0;
        }
    }
}

//  =========================================

sf::FloatRect PistolBullet::getRect() {
    return sf::FloatRect(end_x, end_y, 2, 2);
}

//  ==================================================================================
//  ==================================================================================

HandBullet::HandBullet(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
                       std::vector<ACTOR_PTR> &ActorsStack, std::vector<sf::Texture> &texture, GameContext &context_,
                       sf::Vector2f target, ActorParams &autor, std::vector<ITEMS_PTR> &Items)

        : Actor(position, obj, ActorsPushQueue, ActorsStack, texture, context_, Items), autor(autor) {

    actorParams_.actionsAmount = 1;
    actorParams_.actionsViewsAmount.resize(actorParams_.actionsAmount);
    actorParams_.actionsViewsAmount[(int)Actors::Action::Walk] = 1;
    actorParams_.maxViews = 1;
    actorParams_.dirrectionsAmount = 1;
    actorParams_.actionsAmount = 1;
    target_x = target.x;
    target_y = target.y;
    actorParams_.x = position.x + autor.width/2;
    actorParams_.y = position.y + autor.height/2;
    actorParams_.width = 30;
    actorParams_.height = 30;
    actorParams_.health = 1;
    actorParams_.damage = 200;
    actorParams_.dx = 0;
    actorParams_.dy = 0;
    actorParams_.name = "bullet";
    actorParams_.mIsMovingUp = false;
    actorParams_.mIsMovingDown = false;
    actorParams_.mIsMovingLeft = false;
    actorParams_.mIsMovingRight = false;
    actorParams_.speed = 250;
    fightLive = sf::seconds(1.f / 2.f);
    fightLiveTimer = sf::Time::Zero;

    if (autor.name == "MainHero") {
        set_dirrection();
    } else {
        npc_set_dirrection();
    }

};

//  =========================================

bool HandBullet::set_dirrection() {
    sf::Vector2u size = actorParams_.context.mWindow->getSize();
    angle = atan(fabs((target_x - size.x/2)/(target_y - size.y/2)));
    if (int(target_x - size.x/2) >= 0) {
        actorParams_.mIsMovingRight = true;
    } else if (int(target_x - size.x/2) < 0) {
        actorParams_.mIsMovingLeft = true;
    }

    if (int(target_y - size.y/2) >= 0) {
        actorParams_.mIsMovingDown = true;
    } else if (int(target_y - size.y/2) < 0) {
        actorParams_.mIsMovingUp = true;
    }
}

//  =========================================

bool HandBullet::npc_set_dirrection() {
    sf::Vector2f bullet(actorParams_.x, actorParams_.y);
    angle = atan(fabs((target_x - bullet.x)/(target_y - bullet.y)));
    if (int(target_x - bullet.x) >= 0) {
        actorParams_.mIsMovingRight = true;
    } else if (int(target_x - bullet.x) < 0) {
        actorParams_.mIsMovingLeft = true;
    }

    if (int(target_y - bullet.y) >= 0) {
        actorParams_.mIsMovingDown = true;
    } else if (int(target_y - bullet.y) < 0) {
        actorParams_.mIsMovingUp = true;
    }
}

//  =========================================

bool HandBullet::update_actor(sf::Time deltaTime) {
    fightLiveTimer += fightClock.restart();
    if (fightLiveTimer >= fightLive) {
        actorParams_.health = 0;
        return false;
    }
    if (autor.name == "MainHero") {
        sf::Vector2u size = actorParams_.context.mWindow->getSize();
        angle = atan(fabs((target_x - size.x / 2) / (target_y - size.y / 2)));
    } else {
        sf::Vector2f bullet(actorParams_.x, actorParams_.y);
        angle = atan(fabs((target_x - bullet.x)/(target_y - bullet.y)));
    }

    if (actorParams_.mIsMovingUp) {
        actorParams_.dy = -(actorParams_.speed * cos(angle));
        end_y = (actorParams_.y - actorParams_.height/2) - actorParams_.width * cos(angle);
    }

    if (actorParams_.mIsMovingDown) {
        actorParams_.dy = actorParams_.speed * cos(angle);
        end_y = (actorParams_.y - actorParams_.height/2) + actorParams_.width * cos(angle);
    }

    if (actorParams_.mIsMovingLeft) {
        actorParams_.dx = -(actorParams_.speed * sin(angle));
        end_x = actorParams_.x - actorParams_.width * sin(angle);
    }

    if (actorParams_.mIsMovingRight) {
        actorParams_.dx = actorParams_.speed * sin(angle);
        end_x = actorParams_.x + actorParams_.width * sin(angle);
    }

    actorParams_.x = actorParams_.x + (actorParams_.dx * deltaTime.asSeconds());
    actorParams_.y = actorParams_.y + (actorParams_.dy * deltaTime.asSeconds());

    actorParams_.dx = 0;
    actorParams_.dy = 0;

    checkCollisionWithMap(0, 0);
    checkCollisionWithActors(0, 0);
    checkCollisionWithInteractive(0, 0);
}

//  =========================================

bool HandBullet::checkCollisionWithMap(float Dx, float Dy) {
    sf::FloatRect endRect(end_x, end_y, 2, 2);
    for (int i = 0; i < obj_.size(); i++)
        if (endRect.intersects(obj_[i].rect))
        {
            if (obj_[i].type == "block") {
                actorParams_.health = 0;
            }
        }
}

//  =========================================

bool HandBullet::checkCollisionWithActors(float Dx, float Dy) {
    for (auto actor = ActorsStack_.begin(); actor != ActorsStack_.end(); actor++) {     // обходим стэк
        sf::FloatRect target = (*actor)->getRect();
        sf::FloatRect begin(autor.x, autor.y, autor.width, autor.height);
        if ((target.intersects(this->getRect())) && (begin != target) && (this->getRect() != target) && ((*actor)->get_params().health > 0)
            && ((*actor)->get_params().name != "bullet") && ((*actor)->get_params().name != autor.name)) {
            (*actor)->take_damage(actorParams_.damage);
            actorParams_.health = 0;
        }
    }
}

//  =========================================

bool HandBullet::checkCollisionWithInteractive(float Dx, float Dy) {
    sf::FloatRect ggRect;
    for (int i = 0; i < Items.size(); i++) {                            //проходимся по объектам
        sf::FloatRect target = Items[i]->getRect();
        sf::FloatRect begin(autor.x, autor.y, autor.width, autor.height);
        if (target.intersects(this->getRect()) && (Items[i]->get_name() == "closedDoor")) {
            Items[i]->take_damage(actorParams_.damage);
            actorParams_.health = 0;
        }
    }
}

//  =========================================

bool HandBullet::take_damage(int damage) {
    actorParams_.health -= damage;
}

//  =========================================

sf::FloatRect HandBullet::getRect() {
    return sf::FloatRect(actorParams_.x, actorParams_.y, actorParams_.width, actorParams_.height);
}




