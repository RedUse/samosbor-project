//
// Created by anita on 12/10/18.
//

#include "IntroState.h"
#include "AllStates.h"

using std::make_unique;
using std::move;
using std::string;
using std::cout;
using std::endl;
using STATE_PTR = std::unique_ptr<State>;

IntroState::IntroState(StateManager *stack, GameContext &context_) : State(stack, context_) {}

//  =========================================

bool IntroState::update_game(sf::Time deltaTime) {
    return true;
}

//  =========================================

bool IntroState::draw_game() {
    sf::Texture Texture1, menuTexture2, menuTexture3, helpTexture, commander;
    Texture1.loadFromFile("../GameGraphics/skip.png");
    commander.loadFromFile("../GameGraphics/com.png");

    sf::Sprite skip(Texture1), com(commander);

    skip.setPosition(500, 620);
    skip.setColor(sf::Color::White);

    com.setPosition(280, 0);

    sf::Text hero;
    sf::Font font;
    font.loadFromFile("../GameGraphics/font.ttf");
    hero.setFont(font);
    hero.setString(std::wstring(L"Да, командир"));
    hero.setCharacterSize(30);
    hero.setColor(sf::Color::White);
    hero.setPosition(560, 638);


    sf::Text com_text;
    com_text.setFont(font);
    com_text.setString(std::wstring(L"        Я пребывал в натуральном ступоре, обдумывая прошлое задание: \n"
                                    "            Леха мог бы успеть уйти с нами до закрытия гермодвери. \n"
                                    "             Эта наввязчивая мысль буквально поселилась в голове. \n"
                                    "       Я бы, наверное, стоял так и дальше, если бы не крик командира..."));
    com_text.setCharacterSize(30);
    com_text.setColor(sf::Color::White);
    com_text.setPosition(250, 470);

    bool skip_flag = false;

    context.mWindow->clear(sf::Color(1, 1, 0));

    if (skip.getGlobalBounds().contains(sf::Vector2f(sf::Mouse::getPosition(*context.mWindow).x, sf::Mouse::getPosition(*context.mWindow).y))) {
        skip.setColor(sf::Color(164, 64, 132));
        skip_flag = true;
        context.mWindow->clear(sf::Color(1, 1, 0));

    }

    if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
        if (skip_flag) handle_game_input(sf::Keyboard::S, true);                                      // hello kostil'
    }

    context.mWindow->draw(com);
    context.mWindow->draw(skip);
    context.mWindow->draw(hero);
    context.mWindow->draw(com_text);

    context.mWindow->display();
}

//  =========================================

bool IntroState::handle_game_input(sf::Keyboard::Key key, bool isPressed) {
    if ((key == sf::Keyboard::Escape) && (isPressed)) {                                                          // Сначала обработать события влияющие на состояние
        STATE_PTR new_state = make_unique<MenuState>(stack, context);
        push_game_state(move(new_state));
        return false;
    }

    if ((key == sf::Keyboard::S) && (isPressed)) {                              // Сначала обработать события влияющие на состояние
        STATE_PTR new_state = make_unique<GameState>(stack, context);
        push_game_state(move(new_state));
        return false;
    }
}

bool IntroState::handle_game_input(sf::Mouse::Button mouse, bool isPressed) {

}
