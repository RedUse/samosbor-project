//
// Created by kekor on 02.12.18.
//

#include "Actor.h"
#define MAP_WID     50
#define MAP_HEI     25
#define SIZE        48

using std::make_unique;
using std::move;
using std::string;
using std::cin;
using ACTOR_STATE_PTR = std::unique_ptr<ActorState>;
using ACTOR_PTR = std::unique_ptr<Actor>;

//  =========================================

Actor::Actor(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
             std::vector<ACTOR_PTR> &ActorsStack, std::vector<sf::Texture> &texture, GameContext &context_,
             std::vector<ITEMS_PTR> &Items)
        : actorParams_(texture, position, context_), obj_(obj), ActorsPushQueue_(ActorsPushQueue), ActorsStack_(ActorsStack),
        Items(Items){

}
//  ========================================= actorTexture_(texture), position_(position), obj_(obj)

void Actor::push_actor_state(ACTOR_STATE_PTR &&new_state) {
    ActorsStatesStack_.push_back(move(new_state));
}

//  =========================================

void Actor::pop_actor_state() {
    ActorsStatesBuf_.push_back(move(ActorsStatesStack_.back()));        // Не забыть добавить очистку
    ActorsStatesStack_.pop_back();
}

//  =========================================

void Actor::push_actor(ACTOR_PTR &&newActor) {
    ActorsPushQueue_.push_back(move(newActor));
}

//  =========================================

ACTOR_STATE_PTR &Actor::get_actor_state() {
    return ActorsStatesStack_.back();
}

//  =========================================



size_t Actor::get_states_size() {
    return (ActorsStatesStack_.size());
}


//  =========================================

ActorParams & Actor::get_params()  {
    return actorParams_;
}

//  =========================================

void Actor::push_actor_gun(WEAPON &&new_gun) {
    weapon.push_back(move(new_gun));
}

//  =========================================

void Actor::switch_safe() {
    if (this->actorParams_.name == "bullet") {
        return;
    }
    for (int i = 0; i < obj_.size(); i++) {
        sf::FloatRect hero = (this->getRect());
        sf::FloatRect distr = (obj_[i].rect);
        if ((obj_[i].type == "safe") && hero.intersects(distr)) {
            actorParams_.SAFE = true;
            actorParams_.district = atoi(obj_[i].name.c_str());
            return;
        }
        if ((obj_[i].type == "unsafe") && hero.intersects(distr)) {
            actorParams_.SAFE = false;
            actorParams_.district = atoi(obj_[i].name.c_str());
            return;
        }
        if ((obj_[i].type == "hall") && hero.intersects(distr)) {
            actorParams_.SAFE = false;
            actorParams_.district = atoi(obj_[i].name.c_str());
            return;
        }
    }
}

//  =========================================

void Actor::init_graph() {
    actorParams_.Graph.resize(MAP_HEI);
    for (int h = 0; h < MAP_HEI; h++) {
        actorParams_.Graph[h].resize(MAP_WID);
    }
}

//  =========================================

void Actor::update_graph() {
    for (int h = 0; h < MAP_HEI; h++) {
        for (int w = 0; w < MAP_WID; w++) {
            int x = w * SIZE;
            int y = h * SIZE;
            sf::FloatRect tile(x, y, SIZE, SIZE);
            bool inter = false;
            for (int i = 0; i < obj_.size(); i++) {
                if ( (tile.intersects(obj_[i].rect)) && (obj_[i].type == "block") ) {
                    GraphPoint point(x, y, true);
                    inter = true;
                    actorParams_.Graph[h][w] = point;
                }
            }
            if (!inter) {
                GraphPoint point(x, y, false);
                actorParams_.Graph[h][w] = point;
            }
        }
    }
}

//  =========================================

bool Actor::calculate_path(sf::Vector2f begin_point, sf::Vector2f end_point, std::vector<sf::Vector2f> &path, int distance) {
    static int range = 0;
    if (((size_t)end_point.y / SIZE >= MAP_HEI) || ((size_t)end_point.x / SIZE >= MAP_WID) || ((size_t)end_point.x / SIZE < 0) || ((size_t)end_point.y / SIZE < 0)
    || ((size_t)begin_point.y / SIZE < 0) || ((size_t)begin_point.x / SIZE < 0) || ((size_t)begin_point.x / SIZE >= MAP_WID) || ((size_t)begin_point.y / SIZE >= MAP_HEI)) {
        range = 0;
        return false;
    }
    std::vector<GraphPoint> newCurrent;
    std::vector<GraphPoint> visited;
    std::vector<GraphPoint> current;


    current.push_back(actorParams_.Graph[(size_t)begin_point.y / SIZE][(size_t)begin_point.x / SIZE]);
    current[0].parent = -1;

    if (actorParams_.Graph[(size_t)end_point.y / SIZE][(size_t)end_point.x / SIZE].block) {
        range = 0;
        return false;
    }

    bool find = false;
    while(!find) {
        for (auto curr = current.begin(); curr != current.end() && (!find); curr++) {
            bool repeat = false;
            bool out_map = false;
            int newX;
            int newY;

            if (!(*curr).block) {
                visited.push_back((*curr));

                newX = (*curr).x + SIZE;
                newY = (*curr).y;
                int map = SIZE * (MAP_WID - 1);
                if ((newX < SIZE * MAP_WID) && (newX > 0) && (newY > 0) && (newY < SIZE * MAP_HEI)) {
                    for (auto vis = visited.begin(); vis != visited.end(); vis++) {
                        if ((newX == (*vis).x) && (newY == (*vis).y)) {
                            repeat = true;
                        }
                    }
                    for (auto next = newCurrent.begin(); next != newCurrent.end(); next++) {
                        if ((newX == (*next).x) && (newY == (*next).y)) {
                            repeat = true;
                        }
                    }
                } else {
                    out_map = true;
                }
                if ((!repeat) && (!out_map)) {
                    if (!actorParams_.Graph[newY / SIZE][newX / SIZE].block) {
                        GraphPoint newPoint(newX, newY, actorParams_.Graph[newY / SIZE][newX / SIZE].block);
                        newPoint.parent = visited.size() - 1;
                        newCurrent.push_back(newPoint);
                    }
                }
                repeat = false;
                out_map = false;

                /////////////////////////

                newX = (*curr).x - SIZE;
                newY = (*curr).y;
                if ((newX < SIZE * MAP_WID) && (newX > 0) && (newY > 0) && (newY < SIZE * MAP_HEI)) {
                    for (auto vis = visited.begin(); vis != visited.end(); vis++) {
                        if ((newX == (*vis).x) && (newY == (*vis).y)) {
                            repeat = true;
                        }
                    }
                    for (auto next = newCurrent.begin(); next != newCurrent.end(); next++) {
                        if ((newX == (*next).x) && (newY == (*next).y)) {
                            repeat = true;
                        }
                    }
                } else {
                    out_map = true;
                }
                if ((!repeat) && (!out_map)) {
                    if (!actorParams_.Graph[newY / SIZE][newX / SIZE].block) {
                        GraphPoint newPoint(newX, newY, actorParams_.Graph[newY / SIZE][newX / SIZE].block);
                        newPoint.parent = visited.size() - 1;
                        newCurrent.push_back(newPoint);
                    }
                }
                repeat = false;
                out_map = false;

                /////////////////////////

                newX = (*curr).x;
                newY = (*curr).y + SIZE;
                if ((newX < SIZE * MAP_WID) && (newX > 0) && (newY > 0) && (newY < SIZE * MAP_HEI)) {
                    for (auto vis = visited.begin(); vis != visited.end(); vis++) {
                        if ((newX == (*vis).x) && (newY == (*vis).y)) {
                            repeat = true;
                        }
                    }
                    for (auto next = newCurrent.begin(); next != newCurrent.end(); next++) {
                        if ((newX == (*next).x) && (newY == (*next).y)) {
                            repeat = true;
                        }
                    }
                } else {
                    out_map = true;
                }
                if ((!repeat) && (!out_map)) {
                    if (!actorParams_.Graph[newY / SIZE][newX / SIZE].block) {
                        GraphPoint newPoint(newX, newY, actorParams_.Graph[newY / SIZE][newX / SIZE].block);
                        newPoint.parent = visited.size() - 1;
                        newCurrent.push_back(newPoint);
                    }
                }
                repeat = false;
                out_map = false;

                /////////////////////////

                newX = (*curr).x;
                newY = (*curr).y - SIZE;
                if ((newX < SIZE * MAP_WID) && (newX > 0) && (newY > 0) && (newY < SIZE * MAP_HEI)) {
                    for (auto vis = visited.begin(); vis != visited.end(); vis++) {
                        if ((newX == (*vis).x) && (newY == (*vis).y)) {
                            repeat = true;
                        }
                    }
                    for (auto next = newCurrent.begin(); next != newCurrent.end(); next++) {
                        if ((newX == (*next).x) && (newY == (*next).y)) {
                            repeat = true;
                        }
                    }
                } else {
                    out_map = true;
                }
                if ((!repeat) && (!out_map)) {
                    if (!actorParams_.Graph[newY / SIZE][newX / SIZE].block) {
                        GraphPoint newPoint(newX, newY, actorParams_.Graph[newY / SIZE][newX / SIZE].block);
                        newPoint.parent = visited.size() - 1;
                        newCurrent.push_back(newPoint);
                    }
                }
                repeat = false;
                out_map = false;

                /////////////////////////

                if (((*curr).x == end_point.x) && ((*curr).y == end_point.y)) {
                    find = true;
                }
            }
        }
        current.clear();
        if (find) {
            int end = 0;
            GraphPoint lastPath(visited.back().x, visited.back().y, true);
            lastPath.parent = visited.back().parent;
            bool one_point = false;
            while (lastPath.parent != (-1)) {
                sf::Vector2f pathElem(lastPath.x, lastPath.y);
                path.push_back(pathElem);
                lastPath = visited[lastPath.parent];
                one_point = true;
            }
            range = 0;
            if (one_point)
                return true;
            return false;
        } else {
            for (auto curr = newCurrent.begin(); curr != newCurrent.end(); curr++) {
                current.push_back((*curr));
            }
            newCurrent.clear();
        }

        range++;
        if (range > distance) {
            range = 0;
            return false;
        }

    }
}

//  =========================================

sf::FloatRect Actor::get_inter_collision_rect() {
    return sf::FloatRect(actorParams_.x + actorParams_.width * 1/4, actorParams_.y + actorParams_.height * 1/4,
            actorParams_.width/2, actorParams_.height/2);
}


















