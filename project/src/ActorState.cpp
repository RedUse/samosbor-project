//
// Created by kekor on 02.12.18.
//

#include "Actor.h"
#include "ActorState.h"

using std::make_unique;
using std::move;
using ACTOR_PTR = std::unique_ptr<Actor>;


//  =========================================

ActorState::ActorState(Actor *actor, ActorParams &actorParams_) : actor_(actor), actorParams_(actorParams_) {
}

//  =========================================

void ActorState::push_state(ACTOR_STATE_PTR &&NewState) const {
    actor_->push_actor_state(move(NewState));
}

//  =========================================

void ActorState::pop_state() const {
    actor_->pop_actor_state();
}

//  =========================================

void ActorState::push_actor(ACTOR_PTR &&NewActor) const {
    actor_->push_actor(move(NewActor));
}

//  =========================================


