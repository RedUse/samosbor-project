//
// Created by kekor on 02.12.18.
//
#include "AllActors.h"
#include "AllActorStates.h"

using std::make_unique;
using std::move;
using std::string;
using std::cin;
using ACTOR_STATE_PTR = std::unique_ptr<ActorState>;

//  =========================================

NPC_friend::NPC_friend(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
                       std::vector<ACTOR_PTR> &ActorsStack, std::vector<sf::Texture> &texture, GameContext &context_,
                       std::vector<ITEMS_PTR> &Items)

        : Actor(position, obj, ActorsPushQueue, ActorsStack, texture, context_, Items) {

    actorParams_.actionsAmount = 1;
    actorParams_.actionsWidth.resize(actorParams_.actionsAmount);
    actorParams_.actionsHeight.resize(actorParams_.actionsAmount);
    actorParams_.actionsViewsAmount.resize(actorParams_.actionsAmount);

    actorParams_.actionsViewsAmount[(int)Actors::Action::Walk] = 8;         // Количество видов ходьбы
    actorParams_.actionsWidth[(int)Actors::Action::Walk] = 44;              // Ширина спрайта ходьбы
    actorParams_.actionsHeight[(int)Actors::Action::Walk] = 68;             // Высота спрайта ходьбы
    actorParams_.maxViews = 8;
    actorParams_.dirrectionsAmount = 6;
    actorParams_.width = 44;
    actorParams_.height = 68;
    actorParams_.health = 1000;
    actorParams_.dx = 0;
    actorParams_.dy = 0;
    actorParams_.x = position.x;
    actorParams_.y = position.y;
    actorParams_.name = "npc_friend";
    actorParams_.mIsMovingUp = false;
    actorParams_.mIsMovingDown = false;
    actorParams_.mIsMovingLeft = false;
    actorParams_.mIsMovingRight = false;
    actorParams_.NPCAttac = false;
    actorParams_.speed = 70;

    actorParams_.timeViewChange = sf::seconds(1.f / 8.f);
    actorParams_.timeSinceLastChangeViev = actorParams_.timeViewChange;
    actorParams_.changePathTimer = sf::seconds(1.f / 8.f);
    actorParams_.changePath = actorParams_.changePathTimer;


    /*WEAPON first_gun = make_unique<Pistol>(200);

    float frequency = first_gun->get_temp() * 5;
    actorParams_.fireReaction = sf::seconds(1.f / frequency);
    actorParams_.fireReactionTimer = actorParams_.fireReaction;

    push_actor_gun(move(first_gun));*/

    ACTOR_STATE_PTR first_state = make_unique<NPC_friendStanding>(this, actorParams_);
    push_actor_state(move(first_state));
};




bool NPC_friend::update_actor(sf::Time deltaTime) {
    for (auto actorState = ActorsStatesStack_.rbegin(); actorState != ActorsStatesStack_.rend(); actorState++) {
        switch_safe();
        (*actorState)->update_actor_state(deltaTime);
    }
}


bool NPC_friend::draw_actor() {
    for (auto actorState = ActorsStatesStack_.rbegin();
         actorState != ActorsStatesStack_.rend(); actorState++) {
        if (actorParams_.health > 0) {
            (*actorState)->draw_actor_state();
        } else {
            sf::Sprite smert;
            smert.setTexture(actorParams_.actorTexture[1]);
            smert.setTextureRect(sf::IntRect(0, 0, 70, 39));
            smert.setPosition(actorParams_.x, actorParams_.y + 20);
            actorParams_.context.mWindow->draw(smert);
        }
    }
}


bool NPC_friend::checkCollisionWithMap(float Dx, float Dy) {
    for (int i = 0; i < obj_.size(); i++)
        if (getRect().intersects(obj_[i].rect))
        {
            if (obj_[i].type == "block") {
                if (Dy > 0) {
                    actorParams_.y = obj_[i].rect.top - actorParams_.height;
                    actorParams_.dy = 0;
                    return true;
                }
                if (Dy < 0) {
                    actorParams_.y = obj_[i].rect.top + obj_[i].rect.height;
                    actorParams_.dy = 0;
                    return true;
                }
                if (Dx > 0) {
                    actorParams_.x = obj_[i].rect.left - actorParams_.width;
                    return true;
                }
                if (Dx < 0) {
                    actorParams_.x = obj_[i].rect.left + obj_[i].rect.width;
                    return true;
                }
            }
        }
}

//  =========================================

bool NPC_friend::take_damage(int damage) {
    actorParams_.health -= damage;
}

//  =========================================

sf::FloatRect NPC_friend::getRect() {
    return sf::FloatRect(actorParams_.x, actorParams_.y, actorParams_.width, actorParams_.height);       //эта ф-ция нужна для проверки столкновений
}

