//
// Created by reduse on 16.12.18.
//
#include "SoundPlayer.h"
#include "SoundHolder.h"


SoundPlayer::SoundPlayer()
        : mSoundBuffers()
        , mSounds()
{
    mSoundBuffers.load(SoundEffect::Pistol, "../SoundEffects/PistollightSingle1.wav");
    mSoundBuffers.load(SoundEffect::PistolReload, "../SoundEffects/PistollightReload.wav");
    mSoundBuffers.load(SoundEffect::PistolOutOfAmmo, "../SoundEffects/PistollightOutOfAmmo.wav");
    mSoundBuffers.load(SoundEffect::Usi, "../SoundEffects/SMGheavysingle1.wav");
    mSoundBuffers.load(SoundEffect::UsiReload, "../SoundEffects/SMGheavyReload.wav");
    mSoundBuffers.load(SoundEffect::UsiOutOfAmmo, "../SoundEffects/SMGheavyOutOfAmmo.wav");
    mSoundBuffers.load(SoundEffect::Automat, "../SoundEffects/Riflesingle1.wav");
    mSoundBuffers.load(SoundEffect::AutomatReload, "../SoundEffects/Riflereload.wav");
    mSoundBuffers.load(SoundEffect::AutomatOutOfAmmo, "../SoundEffects/Rifleoutofammo.wav");
    mSoundBuffers.load(SoundEffect::BulletMiss, "../SoundEffects/BulletMiss.wav");
    mSoundBuffers.load(SoundEffect::OpenDoor, "../SoundEffects/OpenDoor.wav");
    mSoundBuffers.load(SoundEffect::CloseDoor, "../SoundEffects/CloseDoor.wav");
    mSoundBuffers.load(SoundEffect::Hand, "../SoundEffects/Hand.wav");
}

void SoundPlayer::play(SoundEffect::ID effect) {
    mSounds.push_back(sf::Sound(mSoundBuffers.get(effect)));
    mSounds.back().play();
    removeStoppedSounds();
}

void SoundPlayer::removeStoppedSounds() {
    mSounds.remove_if([] (const sf::Sound& s) { return s.getStatus() == sf::Sound::Stopped; });
}