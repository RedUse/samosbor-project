#include "StateManager.h"
#include "State.h"
#include "AllStates.h"

using std::make_unique;
using std::move;
using std::string;
using std::cin;
using STATE_PTR = std::unique_ptr<State>;

//  =========================================

StateManager::StateManager(GameContext &context): Context(context) {
	STATE_PTR first_state = make_unique<MenuState>(this, Context);
	push_state(move(first_state));
}
/*При создании, в стек помещается состояние меню, как начальное*/
//  =========================================

void StateManager::push_state(STATE_PTR && new_state) {
	StatesStack.push_back(move(new_state));
}

//  =========================================

void StateManager::pop_state() {
    StatesBuf.push_back(move(StatesStack.back()));
    StatesStack.pop_back();
}
/*Когда состояние внутри себя меняет стэк состояний, вызов pop выдергивает его само
 * из стэка, и происходит неведомая хероммантия, StatesBuf будет всегда хранить 1 только
 * что снятое состояние и уничтожать его когда мы скажем*/

//  =========================================

void StateManager::clear_state() {
    for (auto state = StatesStack.begin(); state != StatesStack.end(); state++) {
        StatesBuf.push_back(move(StatesStack.back()));
        StatesStack.pop_back();
    }
}

//  =========================================

STATE_PTR& StateManager::get_state() {
	return StatesStack.back();
}

//  =========================================
bool StateManager::process_events(const sf::Event &event) {
    bool res = true;
    switch (event.type) {
        case sf::Event::MouseButtonPressed:
            get_state()->handle_game_input(event.mouseButton.button, true);
            break;
        case sf::Event::MouseButtonReleased:
            get_state()->handle_game_input(event.mouseButton.button, false);
            break;
        case sf::Event::KeyPressed:
            get_state()->handle_game_input(event.key.code, true);
            break;
        case sf::Event::KeyReleased:
            get_state()->handle_game_input(event.key.code, false);
            break;
        default:
            break;
    }
    StatesBuf.clear();      // Очищаем временный буффер
}

//  =========================================

bool StateManager::update(sf::Time deltaTime) {
    get_state()->update_game(deltaTime);
}

//  =========================================

bool StateManager::draw() {
    get_state()->draw_game();
}

//  =========================================

size_t StateManager::get_states_size() {
    return (StatesStack.size());
}

//  =========================================


