//
// Created by kekor on 18.12.18.
//

#include "AllActors.h"
#include "AllActorStates.h"

#define PI 3.14159265
#define BAD_DIRRECTION -1
#define MAP_WID     25
#define MAP_HEI     25
#define SIZE        48

using std::make_unique;
using std::move;
using ACTOR_PTR = std::unique_ptr<Actor>;

HelpNpsStanding::HelpNpsStanding(Actor *actor, ActorParams &actorParams_) : ActorState(actor, actorParams_) {

    Sprites_.resize(actorParams_.actionsAmount);
    for (int action = 0; action < actorParams_.actionsAmount; action++) {
        Sprites_[action].resize(actorParams_.dirrectionsAmount);

        for (int dirr = 0; dirr < actorParams_.dirrectionsAmount; dirr++) {
            Sprites_[action][dirr].resize(actorParams_.maxViews);
        }
    }


    for (int action = 0; action < actorParams_.actionsAmount; action++) {
        for (int dirr = 0; dirr < actorParams_.dirrectionsAmount; dirr++) {
            for (int view = 0; view < actorParams_.actionsViewsAmount[action]; view++) {
                sf::Sprite sp;
                sp.setTexture(actorParams_.actorTexture[action]);
                sp.setTextureRect(sf::IntRect(view * actorParams_.actionsWidth[action], dirr * actorParams_.actionsHeight[action],
                                              actorParams_.actionsWidth[action], actorParams_.actionsHeight[action]));
                sp.setPosition(actorParams_.position.x, actorParams_.position.y);
                Sprites_[action][dirr][view] = sp;
            }
        }
    }
    defaultPosition.x = actorParams_.x;
    defaultPosition.y = actorParams_.y;
    target = sf::Vector2f(0, 0);
    targetSuccess = true;
    buttonState = false;
    mouseState = false;
    pos = std::rand() % actorParams_.dirrectionsAmount;
    mPlayer_ = Sprites_[(int)Actors::Action::Walk][pos][0];
    view_ = 0;
    up_variant = int(Actors::Dirrection::Up);
    down_variant = int(Actors::Dirrection::Down);
};

//  =========================================

bool HelpNpsStanding::update_actor_state(sf::Time deltaTime) {
    //std::cout << "NPS:" <<int (actorParams_.x / 48) << ";" << int(actorParams_.y / 48) << std::endl;
    change_weapon();
    float frequency = actor_->weapon[actorParams_.current_weapon]->get_temp() * 5;
    actorParams_.fireReaction = sf::seconds(1.f / frequency);                     // Частота смены выстрела


    sf::FloatRect Rect;
    for (int i = 0; i < actor_->ActorsStack_.size(); i++) {
        if ((actor_->ActorsStack_[i]->get_params().name == "enemy") && (actor_->ActorsStack_[i]->get_params().health > 0)) {      //   Тут подправил
            Rect = actor_->ActorsStack_[i]->getRect();
            float x_diff = (actorParams_.x) - (Rect.left);
            float y_diff = (actorParams_.y) - (Rect.top);
            float current_distance = sqrtf(powf(std::abs(x_diff), 2) + powf(std::abs(y_diff), 2));   //TO DO: Мерить от центра спрайта

            int attacRange;
            if (actorParams_.current_weapon == (int)Weapon::Gun::Hand) {
                if (actorParams_.mIsMovingUp || actorParams_.mIsMovingDown)
                    attacRange = actorParams_.sideRange;
                else
                    attacRange = actorParams_.Range;
            } else {
                attacRange = SIZE * 5;
            }

            if (current_distance < attacRange) {
                if (!check_attac(true, actor_->ActorsStack_[i]->get_params())) {
                    return false;
                }
                actorParams_.mIsMovingLeft = false;
                actorParams_.mIsMovingUp = false;
                actorParams_.mIsMovingRight = false;
                actorParams_.mIsMovingDown = false;
                return false;
            } else {
                if (!check_attac(false, actor_->ActorsStack_[i]->get_params())) {
                    return false;
                }

                for (int i = 0; i < actor_->ActorsStack_.size(); i++) {
                    if (actor_->ActorsStack_[i]->get_params().name == "MainHero") {
                        Rect = actor_->ActorsStack_[i]->getRect();
                    }
                }
                float x_d = (actorParams_.x) - (Rect.left);
                float y_d = (actorParams_.y) - (Rect.top);
                float actor_distance = sqrtf(powf(std::abs(x_d), 2) + powf(std::abs(y_d), 2));

                if ((targetSuccess) && (actor_distance > 75)) {
                    int bX, bY;
                    int eX, eY;

                    if ((int)actorParams_.x % SIZE > SIZE / 2) {
                        bX = int(actorParams_.x / SIZE + 1);
                    } else {
                        bX = int(actorParams_.x / SIZE);
                    }

                    if ((int)actorParams_.y % SIZE > SIZE / 2) {
                        bY = int(actorParams_.y / SIZE + 1);
                    } else {
                        bY = int(actorParams_.y / SIZE);
                    }

                    if ((int)Rect.left % SIZE > SIZE / 2) {
                        eX = int(Rect.left / SIZE + 1);
                    } else {
                        eX = int(Rect.left / SIZE);
                    }

                    if ((int)Rect.top % SIZE > SIZE / 2) {
                        eY = int(Rect.top / SIZE + 1);
                    } else {
                        eY = int(Rect.top / SIZE);
                    }

                    sf::Vector2f begin(bX * SIZE, bY * SIZE);
                    sf::Vector2f end(eX * SIZE, eY * SIZE);
                    std::vector<sf::Vector2f> Path;
                    bool success = actor_->calculate_path(begin, end, Path, 12);
                    if (success && actorParams_.following) {
                        target = (Path.back());
                    } else {
                        bool success = actor_->calculate_path(begin, defaultPosition, Path, 20);
                        if (success) {
                            target = (Path.back());
                        }
                        else {
                            target.x = actorParams_.x;
                            target.y = actorParams_.y;
                        }
                    }
                } else return false;
            }
        } else {
            for (int i = 0; i < actor_->ActorsStack_.size(); i++) {
                if (actor_->ActorsStack_[i]->get_params().name == "MainHero") {
                    Rect = actor_->ActorsStack_[i]->getRect();
                }
            }

            if (targetSuccess) {
                int bX, bY;
                int eX, eY;

                if ((int) actorParams_.x % SIZE > SIZE / 2) {
                    bX = int(actorParams_.x / SIZE + 1);
                } else {
                    bX = int(actorParams_.x / SIZE);
                }

                if ((int) actorParams_.y % SIZE > SIZE / 2) {
                    bY = int(actorParams_.y / SIZE + 1);
                } else {
                    bY = int(actorParams_.y / SIZE);
                }

                if ((int) Rect.left % SIZE > SIZE / 2) {
                    eX = int(Rect.left / SIZE + 1);
                } else {
                    eX = int(Rect.left / SIZE);
                }

                if ((int) Rect.top % SIZE > SIZE / 2) {
                    eY = int(Rect.top / SIZE + 1);
                } else {
                    eY = int(Rect.top / SIZE);
                }

                sf::Vector2f begin(bX * SIZE, bY * SIZE);
                sf::Vector2f end(eX * SIZE, eY * SIZE);
                std::vector<sf::Vector2f> Path;
                bool success = actor_->calculate_path(begin, end, Path, 15);
                if (success && actorParams_.following) {
                    target = (Path.back());
                } else {
                    bool success = actor_->calculate_path(begin, defaultPosition, Path, 7);
                    if (success) {
                        target = (Path.back());
                    }
                    else {
                        target.x = actorParams_.x;
                        target.y = actorParams_.y;
                    }
                }
            }
        }
    }

    float x_npc = actorParams_.x;
    float y_npc = actorParams_.y;
    float x_gg = target.x;
    float y_gg = target.y;

    if ((x_npc == x_gg) && (y_npc == y_gg)) {
        mPlayer_ = Sprites_[(int)Actors::Action::Walk][pos][0];
        mPlayer_.setPosition(actorParams_.x, actorParams_.y);
        targetSuccess = true;
        return false;
    }

    targetSuccess = true;
    npc_handle(Rect);
    change_view(actorParams_, view_);
    move_actor(deltaTime);

    //actor_->checkCollisionWithMap(actorParams_.dx, 0);
    //actor_->checkCollisionWithMap(0, actorParams_.dy);
    actor_->checkCollisionWithActors(actorParams_.dx, 0);
    actor_->checkCollisionWithActors(0, actorParams_.dy);
    actor_->checkCollisionWithInteractive(actorParams_.dx, 0);
    actor_->checkCollisionWithInteractive(0, actorParams_.dy);

    mPlayer_.setPosition(actorParams_.x, actorParams_.y);

    actorParams_.dx = 0;
    actorParams_.dy = 0;
}

//  =========================================

bool HelpNpsStanding::draw_actor_state() {
    actorParams_.context.mWindow->draw(mPlayer_);
}

//  =========================================

int HelpNpsStanding::handle_attac(int weapon, std::string actor_name, ActorParams &target) {
    int dirrection = BAD_DIRRECTION;
    if (fabs(target.y - actorParams_.y) > fabs(target.x - actorParams_.x)) {
        if ((target.y - actorParams_.y) <= 0) {
            if ((buttonState) && (!actorParams_.mIsMovingUp)) {
                return BAD_DIRRECTION;
            }

            if ((target.x - actorParams_.x) > 0) {
                up_variant = 0;
            } else {
                up_variant = 5;
            }

            dirrection = up_variant;
        } else {
            if ((buttonState) && (!actorParams_.mIsMovingDown)) {
                return BAD_DIRRECTION;
            }

            if ((target.x - actorParams_.x) > 0) {
                down_variant = 2;
            } else {
                down_variant = 3;
            }

            dirrection = down_variant;
        }
    } else {
        if ((target.x - actorParams_.x) <= 0) {
            if ((buttonState) && (!actorParams_.mIsMovingLeft)) {
                return BAD_DIRRECTION;
            }
            dirrection = int(Actors::Dirrection::Left);
        } else {
            if ((buttonState) && (!actorParams_.mIsMovingRight)) {
                return BAD_DIRRECTION;
            }
            dirrection = int(Actors::Dirrection::Right);
        }
    }

    sf::Vector2f targetPos(target.x + target.width/2, target.y + target.height/2);
    if ((weapon == (int)Weapon::Gun::Pistol) || (weapon == (int)Weapon::Gun::Automat) || (weapon == (int)Weapon::Gun::Usi)) {
        ACTOR_PTR bullet = make_unique<PistolBullet>(actorParams_.position, actor_->obj_, actor_->ActorsPushQueue_,
                                                     actor_->ActorsStack_, actorParams_.context.allUnitsTextures_.get_unit_texture(Actors::Unit::PistolBullet),
                                                     actorParams_.context, targetPos, actorParams_, actor_->Items);
        actor_->push_actor(move(bullet));
    } else if (weapon == (int)Weapon::Gun::Hand) {
        ACTOR_PTR bullet = make_unique<HandBullet>(actorParams_.position, actor_->obj_, actor_->ActorsPushQueue_,
                                                   actor_->ActorsStack_, actorParams_.context.allUnitsTextures_.get_unit_texture(Actors::Unit::PistolBullet),
                                                   actorParams_.context, targetPos, actorParams_, actor_->Items);
        actor_->push_actor(move(bullet));
    }

    return dirrection;
}

//  =========================================

bool HelpNpsStanding::npc_handle(sf::FloatRect Rect) {
    float x_npc = actorParams_.x;
    float y_npc = actorParams_.y;
    float x_gg = target.x;
    float y_gg = target.y;


    actorParams_.changePath += actorParams_.changePathClock.restart();
    if (actorParams_.changePath > actorParams_.changePathTimer) {

        actorParams_.changePath = sf::Time::Zero;


        if (std::abs(x_npc - x_gg) <= std::abs(y_npc - y_gg)) {
            if (y_gg <= y_npc) {                                         // up
                actorParams_.mIsMovingUp = true;
                actorParams_.mIsMovingDown = false;
                actorParams_.mIsMovingLeft = false;
                actorParams_.mIsMovingRight = false;
            } else {                                                      // down
                actorParams_.mIsMovingDown = true;
                actorParams_.mIsMovingUp = false;
                actorParams_.mIsMovingLeft = false;
                actorParams_.mIsMovingRight = false;
            }
        } else {
            if (x_gg <= x_npc) {                                         // left
                actorParams_.mIsMovingLeft = true;
                actorParams_.mIsMovingRight = false;
                actorParams_.mIsMovingDown = false;
                actorParams_.mIsMovingUp = false;
            } else {                                                      // right
                actorParams_.mIsMovingRight = true;
                actorParams_.mIsMovingLeft = false;
                actorParams_.mIsMovingDown = false;
                actorParams_.mIsMovingUp = false;
            }
        }
    }
}

//  =========================================

void HelpNpsStanding::change_view(ActorParams &actorParams_, int &view_) {
    actorParams_.timeSinceLastChangeViev += actorParams_.clock.restart();
    if (actorParams_.timeSinceLastChangeViev > actorParams_.timeViewChange) {
        if (++view_ == actorParams_.actionsViewsAmount[(int)Actors::Action::Walk]) {
            view_ = 0;
        }
        actorParams_.timeSinceLastChangeViev = sf::Time::Zero;
    }
}

//  =========================================

void HelpNpsStanding::move_actor(sf::Time deltaTime) {
    if (actorParams_.mIsMovingUp) {
        actorParams_.dx = 0;
        actorParams_.dy = -actorParams_.speed;
        if (view_ <= actorParams_.actionsViewsAmount[(int)Actors::Action::Walk]/2 - 1) {
            up_variant = 5;
        } else {
            up_variant = 0;
        }
        mPlayer_ = Sprites_[(int)Actors::Action::Walk][up_variant][view_];
    }

    if (actorParams_.mIsMovingDown) {
        actorParams_.dx = 0;
        actorParams_.dy = actorParams_.speed;

        if (view_ <= actorParams_.actionsViewsAmount[(int)Actors::Action::Walk]/2 - 1) {
            down_variant = 3;
        } else {
            down_variant = 2;
        }

        mPlayer_ = Sprites_[(int)Actors::Action::Walk][down_variant][view_];
    }

    if (actorParams_.mIsMovingLeft) {
        actorParams_.dx = -actorParams_.speed;
        actorParams_.dy = 0;

        mPlayer_ = Sprites_[(int)Actors::Action::Walk][int(Actors::Dirrection::Left)][view_];
    }

    if (actorParams_.mIsMovingRight) {
        actorParams_.dx = actorParams_.speed;
        actorParams_.dy = 0;

        mPlayer_ = Sprites_[(int)Actors::Action::Walk][int(Actors::Dirrection::Right)][view_];
    }

    actorParams_.x = actorParams_.x + (actorParams_.dx * deltaTime.asSeconds());
    actorParams_.y = actorParams_.y + (actorParams_.dy * deltaTime.asSeconds());
    actorParams_.position.x = actorParams_.x;
    actorParams_.position.y = actorParams_.y;
}

//  =========================================

bool HelpNpsStanding::check_attac(bool attacState, ActorParams &target) {
    if (attacState) {
        if (actor_->weapon[actorParams_.current_weapon]->fire()) {
            dirrection = handle_attac(actor_->weapon[actorParams_.current_weapon]->get_name(), actorParams_.name, target);
        }
        if ((dirrection != BAD_DIRRECTION) && (actor_->weapon[actorParams_.current_weapon]->get_ammo())) {
            anime(actor_->weapon[actorParams_.current_weapon]->get_name(), dirrection, actor_->weapon[actorParams_.current_weapon]->check_coolDown());
        }
    } else {
        if (dirrection != BAD_DIRRECTION) {     //Переход в исходное и задержка при ударе
            return (anime(actor_->weapon[actorParams_.current_weapon]->get_name(), dirrection, attacState));
        }
    }
    return true;
}

//  =========================================

bool HelpNpsStanding::anime(int weapon, int dirr, bool is_fire) {
    if (last_weapon != weapon) {
        last_weapon = weapon;
        fire_view = 1;
        fight_view = 0;
    }
    wasPressed = false;
    if (!is_fire) {
        fire_view = 1;
        //automat_view = 0;
        if (weapon == (int)Weapon::Gun::Hand) {
            if (fight_view < actorParams_.actionsViewsAmount[(int) Actors::Action::Fight]) {
                actorParams_.fireReactionTimer += actorParams_.fireClock.restart();
                if (actorParams_.fireReactionTimer >= actorParams_.fireReaction) {
                    mPlayer_ = Sprites_[(int) Actors::Action::Fight][dirr][fight_view++];
                    mPlayer_.setPosition(actorParams_.x, actorParams_.y);
                    actorParams_.fireReactionTimer = sf::Time::Zero;
                }
                return false;
            }
        }
        actorParams_.fireReactionTimer += actorParams_.fireClock.restart();
        if ((actorParams_.fireReactionTimer >= actorParams_.fireReaction) && (wasPressed)) {
            mPlayer_ = Sprites_[(int) Actors::Action::Walk][dirr][0];
            mPlayer_.setPosition(actorParams_.x, actorParams_.y);
            actorParams_.fireReactionTimer = sf::Time::Zero;
            wasPressed = false;
        }
        return true;
    }


    wasPressed = true;
    if (fire_view == actorParams_.actionsViewsAmount[(int)Actors::Action::Shoot]) {
        fire_view = 0;
    }
    if (fight_view == actorParams_.actionsViewsAmount[(int)Actors::Action::Fight]) {
        fight_view = 0;
    }


    actorParams_.fireReactionTimer += actorParams_.fireClock.restart();
    if (actorParams_.fireReactionTimer >= actorParams_.fireReaction) {
        if (weapon == (int)Weapon::Gun::Pistol)  {
            mPlayer_ = Sprites_[(int) Actors::Action::Shoot][dirr][fire_view++];
        } else if (weapon == (int)Weapon::Gun::Usi) {
            mPlayer_ = Sprites_[(int) Actors::Action::ShootUsi][dirr][fire_view++];
        } else if (weapon == (int)Weapon::Gun::Hand) {
            mPlayer_ = Sprites_[(int) Actors::Action::Fight][dirr][fight_view++];
        } else if (weapon == (int)Weapon::Gun::Automat) {
            mPlayer_ = Sprites_[(int) Actors::Action::ShootAutomat][dirr][automat_view++];
            if (automat_view == actorParams_.actionsViewsAmount[(int)Actors::Action::ShootAutomat]) {
                automat_view = 0;
            }
        }
        mPlayer_.setPosition(actorParams_.x, actorParams_.y);
        actorParams_.fireReactionTimer = sf::Time::Zero;

    }
    return true;
}

//  =========================================

void HelpNpsStanding::change_weapon() {
    if (actor_->weapon[actorParams_.current_weapon]->get_ammo() <= 0) {
        for (auto weap = actor_->weapon.rbegin(); weap != actor_->weapon.rend(); weap++) {     // обходим стэк с вершины
            if ((*weap)->get_ammo() > 0) {
                actorParams_.current_weapon = (*weap)->get_name();
                return;
            }
        }
    }
}

//  =========================================

