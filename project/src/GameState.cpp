#include <project/include/NPC_friend.h>
#include "MusicPlayer.h"
#include "AllStates.h"
#include "Watcher.h"
#include "unistd.h"



using std::make_unique;
using std::move;
using std::string;
using std::cout;
using std::endl;
using STATE_PTR = std::unique_ptr<State>;
using ACTOR_PTR = std::unique_ptr<Actor>;


GameState::GameState(StateManager *stack, GameContext &context_) : State(stack, context_), w(context_, ActorsStack, Items, MapObjects, lvl) {
    lvl.LoadFromFile("../GameGraphics/map/map.tmx");//загрузили в него карту, внутри класса с помощью методов он ее обработает.
    MapObjects = lvl.GetAllObjects();
    find_items();

    ACTOR_PTR mainHero = make_unique<GG>(sf::Vector2f(20 * 48, 21 * 48), MapObjects, ActorsPushQueue, ActorsStack,
            context.allUnitsTextures_.get_unit_texture(Actors::Unit::Hero), context_, Items);


    ACTOR_PTR npc = make_unique<Centaur>(sf::Vector2f(24 * 48, 9 * 48), MapObjects, ActorsPushQueue, ActorsStack,
                                         context.allUnitsTextures_.get_unit_texture(Actors::Unit::Centaur), context_, Items);
    ACTOR_PTR cent = make_unique<Centaur>(sf::Vector2f(24 * 48, 11 * 48), MapObjects, ActorsPushQueue, ActorsStack,
                                         context.allUnitsTextures_.get_unit_texture(Actors::Unit::Centaur), context_, Items);
    ACTOR_PTR cent1 = make_unique<Centaur>(sf::Vector2f(30 * 48, 9 * 48), MapObjects, ActorsPushQueue, ActorsStack,
                                          context.allUnitsTextures_.get_unit_texture(Actors::Unit::Centaur), context_, Items);
    ACTOR_PTR cent2 = make_unique<Centaur>(sf::Vector2f(4 * 48, 10 * 48), MapObjects, ActorsPushQueue, ActorsStack,
                                           context.allUnitsTextures_.get_unit_texture(Actors::Unit::Centaur), context_, Items);
    ACTOR_PTR cent3 = make_unique<Centaur>(sf::Vector2f(30 * 48, 11 * 48), MapObjects, ActorsPushQueue, ActorsStack,
                                           context.allUnitsTextures_.get_unit_texture(Actors::Unit::Centaur), context_, Items);
    ACTOR_PTR cent4 = make_unique<Centaur>(sf::Vector2f(1 * 48, 11 * 48), MapObjects, ActorsPushQueue, ActorsStack,
                                           context.allUnitsTextures_.get_unit_texture(Actors::Unit::Centaur), context_, Items);
    ACTOR_PTR npc1 = make_unique<NPC>(sf::Vector2f(11 * 48, 20 * 48), MapObjects, ActorsPushQueue, ActorsStack,
                                     context.allUnitsTextures_.get_unit_texture(Actors::Unit::NPC), context_, Items);
//    ACTOR_PTR npc2 = make_unique<NPC>(sf::Vector2f(11 * 48, 18 * 48), MapObjects, ActorsPushQueue, ActorsStack,
//                                     context.allUnitsTextures_.get_unit_texture(Actors::Unit::NPC), context_, Items);

    ACTOR_PTR npc3 = make_unique<NPC>(sf::Vector2f(10 * 48, 20 * 48), MapObjects, ActorsPushQueue, ActorsStack,
                                      context.allUnitsTextures_.get_unit_texture(Actors::Unit::NPC), context_, Items);

    ACTOR_PTR npc_friend = make_unique<NPC_friend>(sf::Vector2f(9 * 48, 17 * 48), MapObjects, ActorsPushQueue, ActorsStack,
                                     context.allUnitsTextures_.get_unit_texture(Actors::Unit::NPC_friend), context_, Items);



    push_actor(move(npc));
    push_actor(move(npc1));
    push_actor(move(cent1));
    push_actor(move(cent2));
    push_actor(move(cent3));
    push_actor(move(cent4));
    push_actor(move(npc3));
    push_actor(move(cent));
    push_actor(move(npc_friend));
    push_actor(move(mainHero));
}

//  =========================================

bool GameState::update_game(sf::Time deltaTime) {

    check_safe_districts();
    w.check_first_dialog();
    w.SAMOSBOR_UPDATE();
    context.sound.removeStoppedSounds();

    for (auto actor = ActorsPushQueue.begin(); actor != ActorsPushQueue.end(); actor++) {
        push_actor(move(*actor));
    }

    ActorsPushQueue.clear();
    pop_dead_actors("bullet");
    for (auto actor = ActorsStack.rbegin(); actor != ActorsStack.rend(); actor++) {
        if ((*actor)->get_params().ded) {
                STATE_PTR new_menu = make_unique<MenuState>(stack, context);
                stack->clear_state();
                push_game_state(move(new_menu));
        }
        if ((*actor)->get_params().health > 0) {
            (*actor)->update_actor(deltaTime);
            (*actor)->switch_safe();
                int damage = w.SAMOSBOR_DAMAGE((*actor)->get_params().district);
                if (((*actor)->get_params().name != "bullet") && ((*actor)->get_params().name != "enemy")) {
                    (*actor)->take_damage(damage);
                }
        }
    }
    return true;
}

//  =========================================

bool GameState::draw_game() {
    context.mWindow->clear();
    for (auto doors = Items.begin(); doors != Items.end(); doors++) {
        (*doors)->draw_items();
    }
    lvl.Draw(*context.mWindow);


    for (auto actor = ActorsStack.begin(); actor != ActorsStack.end(); actor++) {
        if ((*actor)->get_params().health <= 0) {
            (*actor)->draw_actor();
        }
    }
    for (auto actor = ActorsStack.begin(); actor != ActorsStack.end(); actor++) {
        if ((*actor)->get_params().health > 0) {
            (*actor)->draw_actor();
        }
    }

    w.draw_hud();
    w.check_mission_done();

    if (w.SAMOSBOR) {
        w.draw_timer();
    }

    if (w.show_second_task && !w.dialog3_ended) {
        w.draw_second_task();
    }
    if (w.dialog3_ended) {
        w.draw_last_task();
    }

    if (w.show_help && !w.comand_dead && !w.SAMOSBOR) {
        w.write_help_dialog();
    }

    if (w.second_dialog && !w.dialog2_ended && !w.comand_dead) {
        w.write_dialog2_go_out();
    }
    if (w.third_dialog && !w.dialog3_ended && !w.comand_dead) {
        w.write_dialog3();
    }
//    if (w.mission_done) {
//        std::cout << "DONE" << std::endl;
//    }

    if (!w.dialog1_ended) {
        w.draw_first_task();
        if (w.first_dialog_choice1_end && !w.comand_dead) {
            w.dialog1_ended = true;
            w.show_second_task = true;
        }
        else if (w.first_dialog_choice2_end && !w.comand_dead) {
            w.dialog1_ended = true;
        }
        else if (w.first_dialog_choice3_end && !w.comand_dead) {
            w.dialog1_ended = true;
            w.show_second_task = true;
        }
        else if (w.first_dialog_choice1_2 && !w.comand_dead) {
            w.first_dialog_gg_comand = false;
            w.write_dialog1_com1_2();
        }
        else if (w.first_dialog_choice1_1 && !w.comand_dead) {
            w.first_dialog_gg_comand = false;
            w.write_dialog1_com1_1();
        }
        else if (w.first_dialog_choice1 && !w.comand_dead) {
            w.first_dialog_gg_comand = false;
            w.write_dialog1_com1();
        }
        else if (w.first_dialog_choice2 && !w.comand_dead) {
            w.first_dialog_gg_comand = false;
            w.write_dialog1_com2();
        }
        else  if (w.first_dialog_choice3 && !w.comand_dead) {
            w.first_dialog_gg_comand = false;
            w.write_dialog1_com3();
        }
        else if (w.first_dialog_gg_comand && !w.comand_dead) {
            w.write_dialog1_com_init();
        }
        else  if (w.show_help && !w.first_dialog_gg_comand) {
            w.write_help_dialog();
        }
    }
    else if (!w.mission) {
        w.mission = true;
        w.first_dialog_gg_comand = false;
    }

    w.draw_hp();
    w.draw_weapon();

    context.mWindow->display();

    return true;
}

//  =========================================

bool GameState::handle_game_input(sf::Keyboard::Key key, bool isPressed) {
    if ((key == sf::Keyboard::Escape)&&(isPressed)) {               // Сначала обработать события влияющие на состояние
        std::cout << "from game to menu" << std::endl;
        STATE_PTR new_state = make_unique<MenuState>(stack, context);
        push_game_state(move(new_state));
        return false;
    }

    for (auto actor = ActorsStack.rbegin(); actor != ActorsStack.rend(); actor++) {     // обходим стэк с вершины
        if ((*actor)->get_params().health > 0) {
            (*actor)->handle_actor_input(key, isPressed);
        }
    }

    return true;
}

//  =========================================

bool GameState::handle_game_input(sf::Mouse::Button mouse, bool isPressed) {
    for (auto actor = ActorsStack.begin(); actor != ActorsStack.end(); actor++) {     // обходим стэк
        if ((*actor)->get_params().health > 0) {
            (*actor)->handle_actor_input(mouse, isPressed);
        }
    }
}

//  =========================================

void GameState::push_actor(ACTOR_PTR &&new_actor) {
    ActorsStack.push_back(move(new_actor));
}

//  =========================================

void GameState::pop_actor() {
    ActorsStack.pop_back();
}

//  =========================================

void GameState::pop_dead_actors(std::string name) {
    for (auto actor = ActorsStack.begin(); actor != ActorsStack.end();) {     // обходим стэк с вершины
        if (((*actor)->get_params().name == "bullet") && ((*actor)->get_params().health <= 0)) {
            ActorsStack.erase(actor);
        } else {
            actor++;
        }
    }
}

//  =========================================

void GameState::find_items() {
    for (int i = 0; i < MapObjects.size(); i++) {
        if (MapObjects[i].name == "closedDoor") {
            for (int j = 0; j < MapObjects.size(); j++) {
                if ((MapObjects[j].type == "safe") && (MapObjects[i].rect.intersects(MapObjects[j].rect))) {
                    ITEMS_PTR door = make_unique<Door>(MapObjects[i].name, MapObjects[i].rect,
                                                       context.allUnitsTextures_.get_item_texture(Actors::Items::Door),
                                                       context, j);
                    Items.push_back(move(door));
                    std::cout << i << std::endl;
                }
            }
        }
    }
}

//  =========================================

void GameState::check_safe_districts() {
    for (int i = 0; i < Items.size(); i++) {
        if (Items[i]->check_open()) {
            MapObjects[Items[i]->get_district()].type = "unsafe";
        } else {
            MapObjects[Items[i]->get_district()].type = "safe";
        }
    }
}



























