//
// Created by kekor on 12.12.18.
//

#include "Weapons.h"
#include <SFML/Audio.hpp>


bool Weapons::check_coolDown() {
    buf = clock.restart();
    coolTimer += buf;
    fireTimer += buf;
    if (coolTimer >= coolDown) {
        return true;
    } else {
        return false;
    }
};


//  =========================================
Pistol::Pistol(int ammo_) : Weapons(ammo_) {
    name = 1;
    coolDown = sf::seconds(4.f);
    f_fireTemp = 2;
    fireTemp = sf::seconds(1.f / f_fireTemp);
    coolTimer = coolDown;
    fireTimer = fireTemp;
    magazine = 12;
}

//  =========================================

bool Pistol::fire() {
    if (ammo > 0) {
        buf = clock.restart();
        coolTimer += buf;
        fireTimer += buf;
        if ((coolTimer >= coolDown) && (fireTimer >= fireTemp)) {
            sound.play(SoundEffect::Pistol);
            magazine--;
            ammo --;
            if (magazine == 0) {
                sound.play(SoundEffect::AutomatReload);
                coolTimer = sf::Time::Zero;
                magazine = 12;
            }
            fireTimer = sf::Time::Zero;
            return true;
        }
    }
    if (ammo == 0) {
        sound.play(SoundEffect::PistolOutOfAmmo);
    }
    return false;
}



//  =========================================
//  =========================================

Automat::Automat(int ammo_) : Weapons(ammo_) {
    name = 2;
    coolDown = sf::seconds(3.f);
    f_fireTemp = 7;
    fireTemp = sf::seconds(1.f / f_fireTemp);
    coolTimer = coolDown;
    fireTimer = fireTemp;
    magazine = 30;
}

//  =========================================

bool Automat::fire() {
    if (ammo > 0) {
        buf = clock.restart();
        coolTimer += buf;
        fireTimer += buf;
        if ((coolTimer >= coolDown) && (fireTimer >= fireTemp)) {
            sound.play(SoundEffect::Automat);
            magazine--;
            ammo --;
            if (magazine == 0) {
                coolTimer = sf::Time::Zero;
                magazine = 30;
                sound.play(SoundEffect::AutomatReload);
            }
            fireTimer = sf::Time::Zero;
            return true;
        }
    }
    if (ammo == 0) {
        sound.play(SoundEffect::AutomatOutOfAmmo);
    }
    return false;
}

//  =========================================
//  =========================================

Hand::Hand(int ammo_) : Weapons(ammo_) {
    name = 0;
    coolDown = sf::seconds(0.f);
    f_fireTemp = 1;
    fireTemp = sf::seconds(1.f / f_fireTemp);
    coolTimer = coolDown;
    fireTimer = fireTemp;
    magazine = 1;
}

//  =========================================

bool Hand::fire() {
    buf = clock.restart();
    coolTimer += buf;
    fireTimer += buf;
    if ((coolTimer >= coolDown) && (fireTimer >= fireTemp)) {
        sound.play(SoundEffect::Hand);
        magazine--;
        if (magazine == 0) {
            coolTimer = sf::Time::Zero;
            magazine = 1;
        }
        fireTimer = sf::Time::Zero;
        return true;
    }
    return false;
}

//  =========================================
//  =========================================

Usi::Usi(int ammo_) : Weapons(ammo_) {
    name = 3;
    coolDown = sf::seconds(2.f);
    f_fireTemp = 4;
    fireTemp = sf::seconds(1.f / f_fireTemp);
    coolTimer = coolDown;
    fireTimer = fireTemp;
    magazine = 22;
}

//  =========================================

bool Usi::fire() {
    if (ammo > 0) {
        buf = clock.restart();
        coolTimer += buf;
        fireTimer += buf;
        if ((coolTimer >= coolDown) && (fireTimer >= fireTemp)) {
            sound.play(SoundEffect::Usi);
            magazine--;
            ammo --;
            if (magazine == 0) {
                sound.play(SoundEffect::AutomatReload);
                coolTimer = sf::Time::Zero;
                magazine = 22;
            }
            fireTimer = sf::Time::Zero;
            return true;
        }
    }
    if (ammo == 0) {
        sound.play(SoundEffect::UsiOutOfAmmo);
    }
    return false;
}


