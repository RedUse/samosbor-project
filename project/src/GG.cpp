//
// Created by kekor on 02.12.18.
//
#include "AllActors.h"
#include "ActorState.h"
#include "AllActorStates.h"

using std::make_unique;
using std::move;
using std::string;
using std::cin;
using ACTOR_STATE_PTR = std::unique_ptr<ActorState>;
using WEAPON = std::unique_ptr<Weapons>;

//  =========================================

GG::GG(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
       std::vector<ACTOR_PTR> &ActorsStack, std::vector<sf::Texture> &texture, GameContext &context_,
       std::vector<ITEMS_PTR> &Items)

        : Actor(position, obj, ActorsPushQueue, ActorsStack, texture, context_, Items) {

    actorParams_.actionsAmount = 4;                                         // Количество действий
    actorParams_.actionsWidth.resize(actorParams_.actionsAmount);
    actorParams_.actionsHeight.resize(actorParams_.actionsAmount);
    actorParams_.actionsViewsAmount.resize(actorParams_.actionsAmount);

    actorParams_.actionsViewsAmount[(int)Actors::Action::Walk] = 8;         // Количество видов ходьбы
    actorParams_.actionsViewsAmount[(int)Actors::Action::Shoot] = 5;        // Количество видов выстрела
    actorParams_.actionsViewsAmount[(int)Actors::Action::Fight] = 5;        // Количество видов выстрела
    actorParams_.actionsViewsAmount[(int)Actors::Action::ShootUsi] = 5;        // Количество видов выстрела
    actorParams_.actionsWidth[(int)Actors::Action::Walk] = 44;              // Ширина спрайта ходьбы
    actorParams_.actionsWidth[(int)Actors::Action::Shoot] = 62;             // Ширина спрайта выстрела
    actorParams_.actionsWidth[(int)Actors::Action::Fight] = 57;
    actorParams_.actionsWidth[(int)Actors::Action::ShootUsi] = 63;
    actorParams_.actionsHeight[(int)Actors::Action::Walk] = 70;             // Высота спрайта ходьбы
    actorParams_.actionsHeight[(int)Actors::Action::Shoot] = 78;            // Ширина спрайта выстрела
    actorParams_.actionsHeight[(int)Actors::Action::Fight] = 70;            // Ширина спрайта выстрела
    actorParams_.actionsHeight[(int)Actors::Action::ShootUsi] = 78;            // Ширина спрайта выстрела
    actorParams_.maxViews = 8;
    actorParams_.dirrectionsAmount = 6;
    actorParams_.width = 44;
    actorParams_.height = 71;
    actorParams_.phisic_width = 28;
    actorParams_.phisic_height = 63;
    actorParams_.health = 3000;
    actorParams_.damage = 100;
    actorParams_.dx = 0;
    actorParams_.dy = 0;
    actorParams_.x = position.x;
    actorParams_.y = position.y;
    actorParams_.name = "MainHero";
    actorParams_.mIsMovingUp = false;
    actorParams_.mIsMovingDown = false;
    actorParams_.mIsMovingLeft = false;
    actorParams_.mIsMovingRight = false;
    actorParams_.speed = 200;
    actorParams_.maxHealth = actorParams_.health;




    actorParams_.timeViewChange = sf::seconds(1.f / 8.f);                   // Частота смены шага
    actorParams_.timeSinceLastChangeViev = actorParams_.timeViewChange;

    ACTOR_STATE_PTR first_state = make_unique<ActorStanding>(this, actorParams_);       // Обязательно в конце!!!
    push_actor_state(move(first_state));

    WEAPON hand = make_unique<Hand>(1);
    WEAPON pistol = make_unique<Pistol>(12);
    WEAPON automat = make_unique<Automat>(0);
    WEAPON usi = make_unique<Usi>(60);

    float frequency = hand->get_temp() * 5;
    actorParams_.fireReaction = sf::seconds(1.f / frequency);                     // Частота смены выстрела
    actorParams_.fireReactionTimer = actorParams_.fireReaction;

    actorParams_.current_weapon = (int)Weapon::Gun::Hand;
    push_actor_gun(move(hand));
    push_actor_gun(move(pistol));
    push_actor_gun(move(automat));
    push_actor_gun(move(usi));

};

//  =========================================

bool GG::handle_actor_input(sf::Keyboard::Key key, bool isPressed) {
    for (auto actorState = ActorsStatesStack_.rbegin(); actorState != ActorsStatesStack_.rend(); actorState++) {     // обходим стэк с вершины
        (*actorState)->handle_actor_state_input(key, isPressed);
    }
}

bool GG::handle_actor_input(sf::Mouse::Button mouse, bool isPressed) {
    for (auto actorState = ActorsStatesStack_.rbegin(); actorState != ActorsStatesStack_.rend(); actorState++) {     // обходим стэк с вершины
        (*actorState)->handle_actor_state_input(mouse, isPressed);
    }
}

//  =========================================

bool GG::update_actor(sf::Time deltaTime) {
    for (auto actorState = ActorsStatesStack_.rbegin(); actorState != ActorsStatesStack_.rend(); actorState++) {     // обходим стэк с вершины
        //std::cout<<actorParams_.SAFE << std::endl;
        //std::cout<<"area:"<<actorParams_.district << std::endl;
        float frequency = weapon[actorParams_.current_weapon]->get_temp() * 5;
        actorParams_.fireReaction = sf::seconds(1.f / frequency);                     // Частота смены выстрела
        (*actorState)->update_actor_state(deltaTime);
    }
}

//  =========================================

bool GG::draw_actor() {
    for (auto actorState = ActorsStatesStack_.rbegin(); actorState != ActorsStatesStack_.rend(); actorState++) {     // обходим стэк с вершины
        if (actorParams_.health > 0) {
            (*actorState)->draw_actor_state();
            setPlayerCoordinateForView(actorParams_.x, actorParams_.y, actorParams_.context);
        } else {
            static float dsize = 0;
            sf::Sprite smert;
            smert.setTexture(actorParams_.actorTexture[4]);
            smert.setTextureRect(sf::IntRect(0, 1, 82, 39));
            smert.setPosition(actorParams_.position.x, actorParams_.position.y + 20);
            actorParams_.context.mWindow->draw(smert);
            if (dsize == 0) {
                actorParams_.context.music.play(Music::YouDed);
            }

            sf::Text exit;

            sf::Font font;
            font.loadFromFile("../GameGraphics/font.ttf");


            exit.setFont(font);
            exit.setString(std::wstring(L"YOU DED"));
            exit.setCharacterSize(40 + dsize);
            exit.setColor(sf::Color(255, 0, 0, 128));

            exit.setOrigin(exit.getLocalBounds().width/2, exit.getLocalBounds().height/2);
            exit.setPosition(actorParams_.context.View_.getCenter());
            actorParams_.context.mWindow->draw(exit);
            if(dsize < 250) {
                ++dsize;
            } else {
                dsize = 0;
                actorParams_.ded = true;
                return false;
            }
        }
    }
}

//  =========================================

bool GG::checkCollisionWithMap(float Dx, float Dy) {
    sf::FloatRect ggRect;
    for (int i = 0; i < obj_.size(); i++)                               //проходимся по объектам
        if ((ggRect = getRect()).intersects(obj_[i].rect))                          //проверяем пересечение игрока с объектом
        {
            if (obj_[i].type == "block") {
                if (Dy>0)    { actorParams_.y = obj_[i].rect.top - actorParams_.height; }
                if (Dy<0)    { actorParams_.y = obj_[i].rect.top + obj_[i].rect.height; }
                if (Dx>0)    { actorParams_.x = obj_[i].rect.left - actorParams_.width; }
                if (Dx<0)    { actorParams_.x = obj_[i].rect.left + obj_[i].rect.width; }
            }
        }
}

//  =========================================

void GG::setPlayerCoordinateForView(float x, float y, GameContext &context) {
    float tempX = x + actorParams_.width/2;
    float tempY = y + actorParams_.height/2;
    context.View_.setCenter(tempX, tempY);
    context.mWindow->setView(context.View_);
}

//  =========================================

bool GG::take_damage(int damage) {
    actorParams_.health -= damage;
}

//  =========================================

bool GG::checkCollisionWithInteractive(float Dx, float Dy) {
    sf::FloatRect ggRect;
    for (int i = 0; i < Items.size(); i++)                               //проходимся по объектам
        if ((ggRect = getRect()).intersects(Items[i]->getRect()))                          //проверяем пересечение игрока с объектом
        {
            if (!Items[i]->check_open()) {
                if (Dy>0)    { actorParams_.y = Items[i]->getRect().top - actorParams_.height;  actorParams_.dy = 0; }      //onGround = true; }
                if (Dy<0)    { actorParams_.y = Items[i]->getRect().top + Items[i]->getRect().height;   actorParams_.dy = 0; }
                if (Dx>0)    { actorParams_.y = Items[i]->getRect().top - actorParams_.height;  actorParams_.dy = 0; }
                if (Dx<0)    { actorParams_.y = Items[i]->getRect().top + Items[i]->getRect().height;   actorParams_.dy = 0; }
            }
        }
}


//  =========================================

sf::FloatRect GG::getRect() {
    return sf::FloatRect(actorParams_.x, actorParams_.y, actorParams_.width, actorParams_.height);       //эта ф-ция нужна для проверки столкновений
}

//  =========================================

bool GG::checkCollisionWithActors(float Dx, float Dy) {
    for (auto actor = ActorsStack_.begin(); actor != ActorsStack_.end(); actor++) {     // обходим стэк
        sf::FloatRect target = (*actor)->getRect();
        sf::FloatRect me = this->getRect();
        if ((target.intersects(me))  && (me != target) && ((*actor)->get_params().health > 0) && ((*actor)->get_params().name != "bullet")
        && ((*actor)->get_params().name != "npc")) {
            if (Dy>0)    { actorParams_.y = target.top - actorParams_.height; }
            if (Dy<0)    { actorParams_.y = target.top + target.height; }
            if (Dx>0)    { actorParams_.x = target.left - actorParams_.width; }
            if (Dx<0)    { actorParams_.x = target.left + target.width; }
        }
    }
}