//
// Created by kekor on 03.12.18.
//

#ifndef SBOR_TEXTUREHOLDER_H
#define SBOR_TEXTUREHOLDER_H

#include <SFML/Graphics.hpp>
#include <../Defines.h>

class TextureHolder {
public:
    explicit TextureHolder();

    std::vector<sf::Texture> &get_unit_texture(Actors::Unit unit);
    sf::Texture &get_item_texture(Actors::Items item);
private:
    std::vector<std::vector<sf::Texture>> UnitsTextures;
    std::vector<sf::Texture> ItemsTextures;
};


#endif //SBOR_TEXTUREHOLDER_H
