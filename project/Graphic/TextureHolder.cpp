//
// Created by kekor on 03.12.18.
//

#include "TextureHolder.h"

TextureHolder::TextureHolder() {
    UnitsTextures.resize(5);

    sf::Texture main;
    main.loadFromFile("../GameGraphics/MainHero/Main_walk.png");
    UnitsTextures[(int)Actors::Unit::Hero].push_back(main);

    main.loadFromFile("../GameGraphics//MainHero/Main_shoot_pistol.png");
    UnitsTextures[(int)Actors::Unit::Hero].push_back(main);

    main.loadFromFile("../GameGraphics//MainHero/Main_fight.png");
    UnitsTextures[(int)Actors::Unit::Hero].push_back(main);

    main.loadFromFile("../GameGraphics//MainHero/Main_shoot_usi.png");
    UnitsTextures[(int)Actors::Unit::Hero].push_back(main);

    main.loadFromFile("../GameGraphics//MainHero/Main_smert.png");
    UnitsTextures[(int)Actors::Unit::Hero].push_back(main);

//  =========================================


    sf::Texture npc;
    npc.loadFromFile("../GameGraphics/NPC/NPC_walk.png");
    UnitsTextures[(int)Actors::Unit::NPC].push_back(npc);

    npc.loadFromFile("../GameGraphics//NPC/NPC_shoot_pistol.png");
    UnitsTextures[(int)Actors::Unit::NPC].push_back(npc);

    npc.loadFromFile("../GameGraphics//NPC/NPC_fight.png");
    UnitsTextures[(int)Actors::Unit::NPC].push_back(npc);

    npc.loadFromFile("../GameGraphics//NPC/NPC_shoot_usi.png");
    UnitsTextures[(int)Actors::Unit::NPC].push_back(npc);

    npc.loadFromFile("../GameGraphics/NPC/NPC_shoot_automat.png");
    UnitsTextures[(int)Actors::Unit::NPC].push_back(npc);

    npc.loadFromFile("../GameGraphics/NPC/NPC_smert.png");
    UnitsTextures[(int)Actors::Unit::NPC].push_back(npc);

//  =========================================

    sf::Texture npc_friend;
    npc_friend.loadFromFile("../GameGraphics/NPC_friend/NPC_friend_walk.png");
    UnitsTextures[(int)Actors::Unit::NPC_friend].push_back(npc_friend);

    npc_friend.loadFromFile("../GameGraphics//NPC_friend/NPC_friend_smert.png");
    UnitsTextures[(int)Actors::Unit::NPC_friend].push_back(npc_friend);

    //  ==========================================

    sf::Texture centaur;
    centaur.loadFromFile("../GameGraphics/enemy/Centaur_walk.png");
    UnitsTextures[(int)Actors::Unit::Centaur].push_back(centaur);

    centaur.loadFromFile("../GameGraphics/enemy/Centaur_fight.png"); //pustishka
    UnitsTextures[(int)Actors::Unit::Centaur].push_back(centaur);

    centaur.loadFromFile("../GameGraphics/enemy/Centaur_fight.png");
    UnitsTextures[(int)Actors::Unit::Centaur].push_back(centaur);

    centaur.loadFromFile("../GameGraphics/enemy/Centaur_smert.png");
    UnitsTextures[(int)Actors::Unit::Centaur].push_back(centaur);

//  ==================================================================================

    sf::Texture bullet;
    bullet.loadFromFile("../GameGraphics/PistolBullet.png");
    UnitsTextures[(int)Actors::Unit::PistolBullet].push_back(bullet);

//  ==================================================================================
//  ==================================================================================

    sf::Texture door;
    door.loadFromFile("../GameGraphics/map/door.png");
    ItemsTextures.push_back(door);
}

//  =========================================



std::vector<sf::Texture> &TextureHolder::get_unit_texture(Actors::Unit unit) {
    return UnitsTextures[(int) unit];
}


sf::Texture &TextureHolder::get_item_texture(Actors::Items item) {
    return ItemsTextures[(int)item];
}

