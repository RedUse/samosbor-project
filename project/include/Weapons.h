//
// Created by kekor on 12.12.18.
//

#ifndef SBOR_WEAPONS_H
#define SBOR_WEAPONS_H

#include <SFML/Graphics.hpp>
#include "SoundPlayer.h"
#include <iostream>

class Weapons {
public:
    explicit Weapons(int ammo_) : ammo(ammo_) { };

    virtual bool fire() = 0;

    int get_name() {return name;};
    bool check_coolDown();
    float get_temp() {return f_fireTemp;};
    int get_ammo() {return ammo;};
    int get_magazine(){if (ammo > magazine) return magazine; else return  ammo;};

    SoundPlayer sound;

protected:
    int name;
    sf::Clock clock;
    sf::Time coolDown;
    float f_fireTemp;
    sf::Time fireTemp;
    sf::Time coolTimer;
    sf::Time fireTimer;
    sf::Time buf;
    int ammo;
    int magazine;
};

//  ==================================================================================

class Pistol : public Weapons  {
public:
    explicit Pistol(int ammo_);
    bool fire() override;
};


//  ==================================================================================

class Automat : public Weapons  {
public:
    explicit Automat(int ammo_);
    bool fire() override;
};

//  ==================================================================================

class Hand : public Weapons  {
public:
    explicit Hand(int ammo_);
    bool fire() override;
};

//  ==================================================================================

class Usi : public Weapons  {
public:
    explicit Usi(int ammo_);
    bool fire() override;
};





















#endif //SBOR_WEAPONS_H
