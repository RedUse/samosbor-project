#ifndef SBOR_STATE_H_
#define SBOR_STATE_H_

#include <iostream>
#include <memory>
#include <vector>
#include <SFML/Graphics.hpp>
#include <GameContext.h>

#include "Defines.h"



class StateManager;

class State {
public:
    using STATE_PTR = std::unique_ptr<State>;

    explicit State(StateManager *stack, GameContext &context_);
    virtual ~State() = default;

    virtual bool handle_game_input(sf::Keyboard::Key key, bool isPressed) = 0;
    virtual bool handle_game_input(sf::Mouse::Button mouse, bool isPressed) = 0;
    virtual bool update_game(sf::Time deltaTime) = 0;
    virtual bool draw_game() = 0;

protected:
    void push_game_state(STATE_PTR &&NewState) const;
    void pop_game_state() const;

    StateManager *const stack;
    GameContext &context;
private:

};

#endif //  INCLUDE_STATE_H_