//
// Created by kekor on 28.11.18.
//
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "MusicPlayer.h"
#include "../Graphic/TextureHolder.h"
#include "SoundPlayer.h"

#ifndef SBOR_GAME_CONTEXT_H
#define SBOR_GAME_CONTEXT_H
struct GraphPoint;
class GameContext {
public:
    explicit GameContext(sf::RenderWindow &wnd);

    sf::RenderWindow *mWindow;
    sf::View View_;
    TextureHolder allUnitsTextures_;  //  Текстуры персонажей
    MusicPlayer music; // Kostil ot Globsa 2
    SoundPlayer sound;
};

#endif //SBOR_GAME_CONTEXT_H
