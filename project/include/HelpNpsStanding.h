//
// Created by kekor on 18.12.18.
//

#ifndef SBOR_HELPNPSSTANDING_H
#define SBOR_HELPNPSSTANDING_H

#include "ActorState.h"

class HelpNpsStanding : public ActorState {
public:
    explicit HelpNpsStanding(Actor *actor, ActorParams &actorParams_);

    bool handle_actor_state_input(sf::Keyboard::Key key, bool isPressed) override {};
    bool handle_actor_state_input(sf::Mouse::Button mouse, bool isPressed) override {};
    bool update_actor_state(sf::Time deltaTime) override;
    bool draw_actor_state() override;
    int handle_attac(int weapon, std::string actor_name, ActorParams &target);

    bool npc_handle(sf::FloatRect Rect);
    bool anime (int weapon, int dirr, bool is_fire);
    void change_view(ActorParams &actorParams_, int &view_);
    void move_actor(sf::Time deltaTime);
    bool check_attac(bool attacState, ActorParams &target);
    void change_weapon();

    sf::Vector2f target;
    sf::Vector2f defaultPosition;
    bool targetSuccess;

    int last_weapon = 0;
    int fire_view = 1;
    int automat_view = 0;
    int fight_view = 0;
    bool wasPressed = false;
    int dirrection = -1;
    //void change_view(ActorParams &actorParams_, int &view_);
};

#endif //SBOR_HELPNPSSTANDING_H
