//
// Created by kekor on 15.12.18.
//

#ifndef SBOR_INTERACTIVEOBJECTS_H
#define SBOR_INTERACTIVEOBJECTS_H

#include <SFML/Graphics.hpp>
#include <iostream>

#include "GameContext.h"

class InteractiveObjects {
public:
    explicit InteractiveObjects(std::string name_, sf::Rect<float> rect_, sf::Texture &texture, GameContext &context_, int distr);

    virtual sf::FloatRect getRect() = 0;
    virtual bool draw_items() = 0;
    void close_door() {state = false;};
    void open_door() {state = true;};
    bool check_open();
    bool take_damage(int damage) {health -= damage;};
    std::string get_name() {return name;};
    int get_district() {return district;};

    std::string name;
    sf::FloatRect rect;
    bool state;
    int health;
    int district;
    bool broken;
    sf::Texture &itemTexture;
    GameContext &context;
};

//  ==================================================================================

class Door : public InteractiveObjects {
public:
    explicit Door(std::string name_, sf::Rect<float> rect_, sf::Texture &texture, GameContext &context_, int distr);

    sf::FloatRect getRect() override;
    bool draw_items() override;

    int spriteWidth;
    int spriteHeight;
    int view_amount;
    std::vector<sf::Sprite> item;
};




#endif //SBOR_INTERACTIVEOBJECTS_H
