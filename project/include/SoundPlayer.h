//
// Created by reduse on 16.12.18.
//

#ifndef SBOR_SOUNDPLAYER_H
#define SBOR_SOUNDPLAYER_H

#include <SFML/Audio.hpp>
#include "SoundHolder.h"
#include <vector>
#include <iostream>
#include <list>

namespace SoundEffect
{
    enum ID
    {
        Pistol,
        PistolReload,
        PistolOutOfAmmo,
        Usi,
        UsiReload,
        UsiOutOfAmmo,
        Automat,
        AutomatReload,
        AutomatOutOfAmmo,
        Hand,
        Button,
        BulletMiss,
        OpenDoor,
        CloseDoor,
    };
}

typedef ResourceHolder<sf::SoundBuffer, SoundEffect::ID> SoundBufferHolder;
class SoundPlayer : private sf::NonCopyable
{
public:
    SoundPlayer();

    void play(SoundEffect::ID effect);

    void load(SoundEffect::ID effect, const std::string& filename);

    //void play(SoundEffect::ID effect, sf::Vector2f position);

    void removeStoppedSounds();
    //void setListenerPosition(sf::Vector2f position);
    //void getListenerPosition() const;
private:
    SoundBufferHolder    mSoundBuffers;
    std::list<sf::Sound> mSounds;
};

#endif //SBOR_SOUNDPLAYER_H
