//
// Created by reduse on 16.12.18.
//

#ifndef SBOR_MUSICPLAYER_H
#define SBOR_MUSICPLAYER_H

#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>

namespace Music
{
    enum ID
    {
        MenuTheme,
        MissionTheme,
        RoflTheme,
        YouDed,
    };
}

class MusicPlayer : private sf::NonCopyable
{
public:
    MusicPlayer();
    void play(Music::ID theme);
    void stop();
    void setPaused(bool paused);
    void setVolume(float volume);
private:
    sf::Music mMusic;
    std::map<Music::ID, std::string> mFilenames;
    float mVolume;
};

#endif //SBOR_MUSICPLAYER_H
