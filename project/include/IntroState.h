//
// Created by anita on 12/10/18.
//

#ifndef SBOR_INTROSTATE_H
#define SBOR_INTROSTATE_H

#include "State.h"
#include "GameEngine.h"

#include <string.h>
#include <iostream>

class StateManager;

class IntroState : public State {
public:
    explicit IntroState(StateManager* stack, GameContext &context_);
    bool handle_game_input(sf::Keyboard::Key key, bool isPressed) override;
    bool handle_game_input(sf::Mouse::Button mouse, bool isPressed) override;
    bool update_game(sf::Time deltaTime) override;
    bool draw_game() override;
};


#endif //SBOR_INTROSTATE_H
