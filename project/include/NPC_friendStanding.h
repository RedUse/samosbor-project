//
// Created by anita on 12/15/18.
//

#ifndef SBOR_NPC_FRIENDSTANDING_H
#define SBOR_NPC_FRIENDSTANDING_H

#include "ActorState.h"

class NPC_friendStanding : public ActorState {
public:
    explicit NPC_friendStanding(Actor *actor, ActorParams &actorParams_);

    bool handle_actor_state_input(sf::Keyboard::Key key, bool isPressed) override {};
    bool handle_actor_state_input(sf::Mouse::Button mouse, bool isPressed) override {};
    bool update_actor_state(sf::Time deltaTime) override;
    bool draw_actor_state() override;

    //void change_view(ActorParams &actorParams_, int &view_);
};

#endif //SBOR_NPC_FRIENDSTANDING_H
