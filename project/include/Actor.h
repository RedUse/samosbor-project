//
// Created by kekor on 02.12.18.
//

#ifndef SBOR_ACTOR_H
#define SBOR_ACTOR_H

#include <SFML/Graphics.hpp>
#include <iostream>
#include <vector>
#include <memory>
#include <iostream>
#include <cmath>

#include "Weapons.h"
#include "GameContext.h"
#include "ActorState.h"
#include "InteractiveObjects.h"



typedef struct GraphPoint {
    GraphPoint(){};
    GraphPoint(int x_, int y_, bool block_) : x(x_), y(y_), block(block_), last(nullptr) {};
    int x;
    int y;
    bool block;
    GraphPoint *last;
    int parent;
}GraphPoint;







/*ВСЕ ПАРАМЕТРЫ АКТЕРА ПЕРЕДАЮТСЯ В СОСТОЯНИЯ СТРУКТУРОЙ*/
typedef struct ActorParams {
    ActorParams(std::vector<sf::Texture> &texture, sf::Vector2f position, GameContext &context_) :
            actorTexture(texture), position(position), context(context_) {};


    sf::Vector2f position;
    std::vector<sf::Texture> &actorTexture;                              //  У каждого актера своя текстура

    sf::Clock clock;
    sf::Time timeSinceLastChangeViev;                       //Шобы картинка челика менялась по своему таймеру
    sf::Time timeViewChange;
    sf::Clock fireClock;
    sf::Time fireReaction;
    sf::Time fireReactionTimer;

    // NPC =========================
    sf::Clock changePathClock;
    sf::Time changePath;
    sf::Time changePathTimer;
    sf::Vector2f firstPosition;
    bool following = false;

    float sideRange;
    float Range;
    float rightRange;
    float leftRange;
    float upRange;
    float downRange;
    bool NPCAttac;
    // NPC =========================



    // GG =========================
    bool ded = false;
    bool Fpressed = false;
    bool Zpressed = false;
    bool Xpressed = false;
    bool Cpressed = false;
    bool can_walking = true;
    // GG =========================

    std::vector<size_t> actionsViewsAmount;
    size_t actionsAmount;
    size_t dirrectionsAmount;                               //  Любые изменения череваты сегфолтом!!!
    size_t maxViews;
    int sprite_w;
    int sprite_h;
    std::vector<size_t> actionsWidth;
    std::vector<size_t> actionsHeight;
    int width;
    int height;

    // Phisic
    int phisic_width;
    int phisic_height;
    float phisic_x;
    float phisic_y;
    // Phisic

    int health;
    int maxHealth;
    int maxSpeed;
    int damage;
    float speed;
    int current_weapon;
    float dx;
    float dy;
    float x;
    float y;
    std::string name;

    bool mIsMovingUp;
    bool mIsMovingDown;
    bool mIsMovingLeft;
    bool mIsMovingRight;

    bool spriteUp;
    bool spriteDown;
    bool spriteLeft;
    bool spriteRight;

    bool SAFE;
    int district;

    std::vector<std::vector<GraphPoint>> Graph;
    std::vector<sf::Vector2f> Path;

    GameContext &context;
}ActorParams ;

//  =========================================

class Actor {
public:
    using WEAPON = std::unique_ptr<Weapons>;
    using ACTOR_STATE_PTR = std::unique_ptr<ActorState>;
    using ACTOR_PTR = std::unique_ptr<Actor>;
    using ITEMS_PTR = std::unique_ptr<InteractiveObjects>;

    explicit Actor(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
                       std::vector<ACTOR_PTR> &ActorsStack, std::vector<sf::Texture> &texture, GameContext &context_,
                   std::vector<ITEMS_PTR> &Items);

    virtual bool handle_actor_input(sf::Keyboard::Key key, bool isPressed) = 0;
    virtual bool handle_actor_input(sf::Mouse::Button mouse, bool isPressed) = 0;
    virtual bool update_actor(sf::Time deltaTime) = 0;
    virtual bool draw_actor() = 0;
    virtual bool checkCollisionWithMap(float Dx, float Dy) = 0;
    virtual bool checkCollisionWithActors(float Dx, float Dy) = 0;
    virtual bool checkCollisionWithInteractive(float Dx, float Dy) = 0;
    virtual bool take_damage(int damage) = 0;
    virtual sf::FloatRect getRect() = 0;

    ACTOR_STATE_PTR &get_actor_state();
    void push_actor_state(ACTOR_STATE_PTR &&new_state);
    void push_actor_gun(WEAPON &&new_gun);
    void push_actor(ACTOR_PTR &&newActor);
    sf::FloatRect get_inter_collision_rect();

    void pop_actor_state();

    void switch_safe();

    size_t get_states_size();
    ActorParams & get_params() ;

    void init_graph();
    void update_graph();
    bool calculate_path(sf::Vector2f begin_point, sf::Vector2f end_point, std::vector<sf::Vector2f> &path, int distance);

    std::vector<Object> &obj_;                  // Добавлена ссыль на вектор объектов мапы
    std::vector<ITEMS_PTR> &Items;
    std::vector<ACTOR_PTR> &ActorsPushQueue_;       // Ссыль на очередь добавления актеров
    std::vector<ACTOR_PTR> &ActorsStack_;       // Ссыль на актеров
    std::vector<WEAPON> weapon;                 //Стэк оружия
    ActorParams actorParams_;
protected:
    std::vector<ACTOR_STATE_PTR> ActorsStatesStack_;
    std::vector<ACTOR_STATE_PTR> ActorsStatesBuf_;
};

#endif //SBOR_ACTOR_H
