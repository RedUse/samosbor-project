#ifndef SBOR_MENU_STATE_H_
#define SBOR_MENU_STATE_H_

#include "State.h"
#include "GameEngine.h"

#include <string.h>
#include <iostream>

class StateManager;

class MenuState : public State {
public:
    explicit MenuState(StateManager* stack, GameContext &context_);
    bool handle_game_input(sf::Keyboard::Key key, bool isPressed) override;
    bool handle_game_input(sf::Mouse::Button mouse, bool isPressed) override {};
    bool update_game(sf::Time deltaTime) override;
    bool draw_game() override;
private:
    sf::Texture menuTexture1, menuTexture2, menuTexture3, helpTexture, menuBackground;
    sf::Sprite menu1, menu2, menu3, help, menuBg;
    int menuNum = 0;
};

#endif //  SBOR_MENU_STATE_H_