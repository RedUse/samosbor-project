#ifndef SBOR_STATEMANAGER_H_
#define SBOR_STATEMANAGER_H_

#include <iostream>
#include <vector>
#include <memory>
#include "State.h"
#include "GameContext.h"


class StateManager
{
    using STATE_PTR = std::unique_ptr<State>;
public:
    explicit StateManager(GameContext &context);

    bool process_events(const sf::Event &event);
    bool update(sf::Time deltaTime);
    bool draw();

    STATE_PTR &get_state();
    void push_state(STATE_PTR &&new_state);
    void pop_state();
    void clear_state();

    size_t get_states_size();


private:
    std::vector<STATE_PTR> StatesStack;
    std::vector<STATE_PTR> StatesBuf;
    GameContext &Context;
};


#endif  //  INCLUDE_STATEMANAGER_H_
