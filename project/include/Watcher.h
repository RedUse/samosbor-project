//
// Created by anita on 12/15/18.
//
#include "GameContext.h"
#include "Actor.h"
#include "InteractiveObjects.h"

#include <string.h>


#ifndef SBOR_WATCHER_H
#define SBOR_WATCHER_H

class Watcher {
    using ACTOR_PTR = std::unique_ptr<Actor>;
    using ITEMS_PTR = std::unique_ptr<InteractiveObjects>;
    bool fog = false;
    std::vector<ACTOR_PTR> &ActorsStack_;
    std::vector<ITEMS_PTR> &Items;
    std::vector<Object> &MapObjects;
    Level &lvl;

public:
    explicit Watcher(GameContext &context, std::vector<ACTOR_PTR> &ActorsStack, std::vector<ITEMS_PTR> &Items, std::vector<Object> &MapObjects, Level &lvl);

    sf::Clock samosborBeginClock;
    sf::Time samosborBegin;
    sf::Time samosborBeginTimer;

    sf::Clock samosborEndClock;
    sf::Time samosborEnd;
    sf::Time samosborEndTimer;
    bool SAMOSBOR = false;
    bool sborBegin = false;
    bool sborEnd = false;
    bool is_samosbor();
    bool check_interactive(sf::FloatRect Rect_gg);
    bool SAMOSBOR_UPDATE();
    int SAMOSBOR_DAMAGE(int layNum);
    bool draw_timer();


    bool show_help = false;
    bool first_dialog_gg_comand = false;

    bool first_dialog_choice1 = false;
    bool first_dialog_choice1_1 = false;
    bool first_dialog_choice1_2 = false;
    bool first_dialog_choice1_end = false;


    bool first_dialog_choice2 = false;
    bool first_dialog_choice2_end = false;

    bool first_dialog_choice3 = false;
    bool first_dialog_choice3_end = false;


    bool dialog1_ended = false;
    bool comand_dead = false;

    bool check_first_dialog();
    bool write_help_dialog();

    bool write_dialog1_com_init();
    bool write_dialog1_com1();
    bool write_dialog1_com1_1();
    bool write_dialog1_com1_2();

    bool write_dialog1_com2();

    bool write_dialog1_com3();

    bool show_second_task = false;
    bool draw_first_task();
    bool draw_second_task();

    bool draw_hud();
    bool draw_hp();
    bool draw_weapon();

    sf::Font font;

    sf::Texture hud;
    sf::Sprite hud_bar;

    sf::Texture com_face;
    sf::Sprite comand_face;

    sf::Text com_name;

    sf::Texture pistol_t;
    sf::Sprite pistol;

    sf::Texture uzi_t;
    sf::Sprite uzi;

    sf::Texture automat_t;
    sf::Sprite automat;

    sf::Texture gg_hud_t;
    sf::Sprite gg_hud;

    sf::Text task_title;

    GameContext &context;

    bool mission = false;
    bool mission_done = false;

    bool write_dialog2_go_out();
    bool check_mission_done();

    bool second_dialog = false;
    bool dialog2_ended = false;

    bool samosbor_restarted = false;

    bool third_dialog = false;
    bool dialog3_ended = false;
    bool write_dialog3();
    bool draw_last_task();



};
#endif //SBOR_WATCHER_H
