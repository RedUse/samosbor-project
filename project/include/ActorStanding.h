//
// Created by kekor on 02.12.18.
//

#ifndef SBOR_STANDINGSTATE_H
#define SBOR_STANDINGSTATE_H

#include "ActorState.h"

class ActorStanding : public ActorState {
public:
    explicit ActorStanding(Actor *actor, ActorParams &actorParams_);

    bool handle_actor_state_input(sf::Keyboard::Key key, bool isPressed) override;
    bool handle_actor_state_input(sf::Mouse::Button mouse, bool isPressed) override;
    bool update_actor_state(sf::Time deltaTime) override;
    bool draw_actor_state() override;
    int handle_attac(int weapon, std::string actor_name);

    bool anime (int weapon, int dirr, bool is_fire);
    void change_view(ActorParams &actorParams_, int &view_);
    void move_actor(sf::Time deltaTime);
    bool check_attac(bool mouseState);
    void change_weapon();
};

#endif //SBOR_STANDINGSTATE_H
