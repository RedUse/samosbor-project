//
// Created by kekor on 09.12.18.
//

#ifndef SBOR_BULLETS_H
#define SBOR_BULLETS_H

#include <string.h>
#include <iostream>

#include "Actor.h"

class PistolBullet : public Actor {
public:
    explicit PistolBullet(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
                          std::vector<ACTOR_PTR> &ActorsStack, std::vector<sf::Texture> &texture, GameContext &context_,
                          sf::Vector2f target, ActorParams &autor, std::vector<ITEMS_PTR> &Items);

    bool handle_actor_input(sf::Keyboard::Key key, bool isPressed) override {};
    bool handle_actor_input(sf::Mouse::Button mouse, bool isPressed) override {};
    bool update_actor(sf::Time deltaTime) override;
    bool draw_actor() override;
    bool checkCollisionWithMap(float Dx, float Dy) override;
    bool checkCollisionWithActors(float Dx, float Dy) override;
    bool checkCollisionWithInteractive(float Dx, float Dy) override;
    bool take_damage(int damage) override;
    sf::FloatRect getRect() override;


    float set_rotation();
    bool npc_set_dirrection();
    bool set_dirrection();
protected:
    double angle;
    float target_x;
    float target_y;
    double end_x;
    double end_y;
    ActorParams &autor;
    std::vector<std::vector<sf::Sprite>> Sprites_;
    sf::Sprite mPlayer_;
};

//  ==================================================================================

class HandBullet : public Actor {
public:
    explicit HandBullet(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
                          std::vector<ACTOR_PTR> &ActorsStack, std::vector<sf::Texture> &texture, GameContext &context_,
                          sf::Vector2f target, ActorParams &autor, std::vector<ITEMS_PTR> &Items);

    bool handle_actor_input(sf::Keyboard::Key key, bool isPressed) override {};
    bool handle_actor_input(sf::Mouse::Button mouse, bool isPressed) override {};
    bool update_actor(sf::Time deltaTime) override;
    bool draw_actor() override {};
    bool checkCollisionWithMap(float Dx, float Dy) override;
    bool checkCollisionWithActors(float Dx, float Dy) override;
    bool checkCollisionWithInteractive(float Dx, float Dy) override;
    bool take_damage(int damage) override;
    sf::FloatRect getRect() override;


    bool npc_set_dirrection();
    bool set_dirrection();
protected:
    sf::Clock fightClock;
    sf::Time fightLive;
    sf::Time fightLiveTimer;
    double angle;
    float target_x;
    float target_y;
    double end_x;
    double end_y;
    ActorParams &autor;
};

#endif //SBOR_BULLETS_H
