//
// Created by den on 16.12.18.
//

#ifndef SBOR_CENTAUR_H
#define SBOR_CENTAUR_H

#include "Actor.h"

#include <string.h>
#include <iostream>

struct GraphPoint;

class Centaur : public Actor {
public:
    explicit Centaur(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
                 std::vector<ACTOR_PTR> &ActorsStack, std::vector<sf::Texture> &texture, GameContext &context_,
                 std::vector<ITEMS_PTR> &Items);

    bool handle_actor_input(sf::Keyboard::Key key, bool isPressed) override {};
    bool handle_actor_input(sf::Mouse::Button mouse, bool isPressed) override {};
    bool update_actor(sf::Time deltaTime) override;
    bool draw_actor() override;
    bool checkCollisionWithMap(float Dx, float Dy) override;
    bool checkCollisionWithActors(float Dx, float Dy) override;
    bool checkCollisionWithInteractive(float Dx, float Dy) override;
    bool take_damage(int damage) override;
    sf::FloatRect getRect() override;

    bool notice = false;
};

#endif //SBOR_CENTAUR_H
