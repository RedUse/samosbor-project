//
// Created by kekor on 02.12.18.
//


#ifndef SBOR_ACTORSSTATES_H
#define SBOR_ACTORSSTATES_H


#include "level.h"
#include "view.h"
#include <iostream>
#include <memory>
#include <vector>
#include <SFML/Graphics.hpp>
#include <GameContext.h>
#include "../Graphic/TextureHolder.h"

class Actor;
struct ActorParams;



class ActorState {
public :
    using ACTOR_STATE_PTR = std::unique_ptr<ActorState>;
    using ACTOR_PTR = std::unique_ptr<Actor>;

    explicit ActorState(Actor *actor, ActorParams &actorParams_);
    virtual ~ActorState() = default;

    virtual bool handle_actor_state_input(sf::Keyboard::Key key, bool isPressed) = 0;
    virtual bool handle_actor_state_input(sf::Mouse::Button mouse, bool isPressed) = 0;
    virtual bool update_actor_state(sf::Time deltaTime) = 0;
    virtual bool draw_actor_state() = 0;

protected:
    void push_state(ACTOR_STATE_PTR &&NewState) const;
    void pop_state() const;

    void push_actor(ACTOR_PTR &&NewState) const;

    bool buttonState;
    bool mouseState;
    std::vector<std::vector<std::vector<sf::Sprite>>> Sprites_;
    sf::Sprite mPlayer_;
    Actor *actor_;
    size_t pos;

    ActorParams &actorParams_;

    int view_;
    int up_variant;
    int down_variant;
};



#endif //SBOR_ACTORSSTATES_H
