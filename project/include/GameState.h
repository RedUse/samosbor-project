#ifndef SBOR_GAMESTATE_H_
#define SBOR_GAMESTATE_H_

#include "State.h"
#include "Watcher.h"
#include "../Graphic/TextureHolder.h"
#include "AllActors.h"
#include "level.h"
#include "view.h"
#include "InteractiveObjects.h"

#include <string.h>
#include <iostream>


class StateManager;

class GameState : public State {
public:
    using ACTOR_PTR = std::unique_ptr<Actor>;
    using ITEMS_PTR = std::unique_ptr<InteractiveObjects>;

    explicit GameState(StateManager *stack, GameContext &context_);

    bool handle_game_input(sf::Keyboard::Key key, bool isPressed) override;
    bool handle_game_input(sf::Mouse::Button mouse, bool isPressed) override;
    bool update_game(sf::Time deltaTime) override;
    bool draw_game() override;

    ACTOR_PTR &get_actor();
    void push_actor(ACTOR_PTR &&new_actor);
    void pop_actor();
    void pop_dead_actors(std::string name);
    Watcher w;

    void find_items();
    void check_safe_districts();

private:
    std::vector<ITEMS_PTR> Items;
    std::vector<ACTOR_PTR> ActorsStack;
    std::vector<ACTOR_PTR> ActorsPushQueue;

    Level lvl;                                          //экземпляр класса уровень
    std::vector<Object> MapObjects;                     //  Вектор объектов карты

};

#endif //  INCLUDE_GAMESTATE_H_