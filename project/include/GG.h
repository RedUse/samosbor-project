//
// Created by kekor on 02.12.18.
//

#ifndef SBOR_GG_H
#define SBOR_GG_H

#include "Actor.h"

#include <string.h>
#include <iostream>

class GG : public Actor {
public:
    explicit GG(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
                std::vector<ACTOR_PTR> &ActorsStack, std::vector<sf::Texture> &texture, GameContext &context_,
                std::vector<ITEMS_PTR> &Items);

    bool handle_actor_input(sf::Keyboard::Key key, bool isPressed) override;
    bool handle_actor_input(sf::Mouse::Button mouse, bool isPressed) override;
    bool update_actor(sf::Time deltaTime) override;
    bool draw_actor() override;
    bool checkCollisionWithMap(float Dx, float Dy) override;
    bool checkCollisionWithActors(float Dx, float Dy) override;
    bool checkCollisionWithInteractive(float Dx, float Dy) override;
    sf::FloatRect getRect() override;

    bool take_damage(int damage) override;

    void setPlayerCoordinateForView(float x, float y, GameContext &context);
private:
};

#endif //SBOR_GG_H
