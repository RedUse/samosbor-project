#include "AllStates.h"
#include "unistd.h"
#include "HelpState.h"

using std::make_unique;
using std::move;
using std::string;
using std::cout;
using std::endl;
using STATE_PTR = std::unique_ptr<State>;

HelpState::HelpState(StateManager *stack, GameContext &context_) :  State(stack, context_) {}

//  =========================================

bool HelpState::update_game(sf::Time /*deltaTime*/) {
    return true;
}

//  =========================================

bool HelpState::draw_game() {
    sf::Texture helpTexture;
    helpTexture.loadFromFile("../GameGraphics/help.png");
    sf::Sprite help(helpTexture);

    context.mWindow->clear(sf::Color(1, 1, 0));


    sf::Font font;
    font.loadFromFile("../GameGraphics/font.ttf");

    sf::Text text;
    text.setFont(font);
    text.setString(std::wstring(L"      Горячие клавиши в меню:\n"
                                "            G - начать игру\n"
                                "            H - помощь\n "
                                "\n "
                                "      Горячие клавиши в игре:\n"
                                "            ESC - выход\n"
                                "            M - обратно в меню\n"));
    text.setCharacterSize(30);
    text.setColor(sf::Color::White);
    text.setPosition(700, 200);


    context.mWindow->draw(help);
    context.mWindow->draw(text);

    context.mWindow->display();

    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
        handle_game_input(sf::Keyboard::Escape, true);
    }
}

//  =========================================

bool HelpState::handle_game_input(sf::Keyboard::Key key, bool isPressed) {
    if ((key == sf::Keyboard::Escape)&&(isPressed)) {
        pop_game_state();
        if (stack->get_states_size() == 0) {
            STATE_PTR new_state = make_unique<MenuState>(stack, context);
            push_game_state(move(new_state));
        }
        return false;
    }
    return true;
}