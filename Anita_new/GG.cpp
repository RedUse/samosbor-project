//
// Created by kekor on 02.12.18.
//
#include "AllActors.h"
#include "ActorState.h"
#include "AllActorStates.h"

using std::make_unique;
using std::move;
using std::string;
using std::cin;
using ACTOR_STATE_PTR = std::unique_ptr<ActorState>;

//  =========================================

GG::GG(sf::Vector2f position, std::vector<Object> &obj, std::vector<ACTOR_PTR> &ActorsPushQueue,
       std::vector<ACTOR_PTR> &ActorsStack, sf::Texture &texture, GameContext &context_)

        : Actor(position, obj, ActorsPushQueue, ActorsStack, texture, context_) {

    actorParams_.viewsAmount = 8;
    actorParams_.dirrectionsAmount = 6;
    actorParams_.width = 44;
    actorParams_.height = 70;
    actorParams_.health = 1000;
    actorParams_.damage = 228;
    actorParams_.dx = 0;
    actorParams_.dy = 0;
    actorParams_.x = position.x;
    actorParams_.y = position.y;
    actorParams_.name = "MainHero";
    actorParams_.weapon = ActorParams::pistol;

    actorParams_.mIsMovingUp = false;
    actorParams_.mIsMovingDown = false;
    actorParams_.mIsMovingLeft = false;
    actorParams_.mIsMovingRight = false;
    actorParams_.speed = 200;

    actorParams_.timeViewChange = sf::seconds(1.f / 8.f);
    actorParams_.timeSinceLastChangeViev = sf::Time::Zero;

    ACTOR_STATE_PTR first_state = make_unique<ActorStanding>(this, actorParams_);       // Обязательно в конце!!!
    push_actor_state(move(first_state));
};

//  =========================================

bool GG::handle_actor_input(sf::Keyboard::Key key, bool isPressed) {
    for (auto actorState = ActorsStatesStack_.rbegin(); actorState != ActorsStatesStack_.rend(); actorState++) {     // обходим стэк с вершины
        (*actorState)->handle_actor_state_input(key, isPressed);
    }
}

bool GG::handle_actor_input(sf::Mouse::Button mouse, bool isPressed) {
    for (auto actorState = ActorsStatesStack_.rbegin(); actorState != ActorsStatesStack_.rend(); actorState++) {     // обходим стэк с вершины
        (*actorState)->handle_actor_state_input(mouse, isPressed);
    }
}

//  =========================================

bool GG::update_actor(sf::Time deltaTime) {
    for (auto actorState = ActorsStatesStack_.rbegin(); actorState != ActorsStatesStack_.rend(); actorState++) {     // обходим стэк с вершины
        (*actorState)->update_actor_state(deltaTime);
    }
}

//  =========================================

bool GG::draw_actor() {
    for (auto actorState = ActorsStatesStack_.rbegin(); actorState != ActorsStatesStack_.rend(); actorState++) {     // обходим стэк с вершины
        if (actorParams_.health > 0) {
            (*actorState)->draw_actor_state();
            setPlayerCoordinateForView(actorParams_.x, actorParams_.y, actorParams_.context);
        } else {
            static float dsize = 0;
            sf::Sprite smert;
            smert.setTexture(actorParams_.actorTexture);
            smert.setTextureRect(sf::IntRect(0,0,0,0));
            smert.setPosition(actorParams_.position.x, actorParams_.position.y);
            actorParams_.context.mWindow->draw(smert);

            sf::Text exit;

            sf::Font font;
            font.loadFromFile("../GameGraphics/font.ttf");

            exit.setFont(font);
            exit.setString(std::wstring(L"YOU DED"));
            exit.setCharacterSize(40 + dsize);
            exit.setColor(sf::Color(255, 0, 0, 128));

            exit.setOrigin(exit.getLocalBounds().width/2, exit.getLocalBounds().height/2);
            exit.setPosition(actorParams_.context.View_.getCenter());
            actorParams_.context.mWindow->draw(exit);
            if(dsize < 300) {
                ++dsize;
            } else {
                return false;
            }

        }
    }
}

//  =========================================

bool GG::checkCollisionWithMap(float Dx, float Dy) {
    for (int i = 0; i < obj_.size(); i++)                               //проходимся по объектам
        if (getRect().intersects(obj_[i].rect))                          //проверяем пересечение игрока с объектом
        {
            if (obj_[i].name == "block")                                 //если встретили препятствие
            {
                if (Dy>0)    { actorParams_.y = obj_[i].rect.top - actorParams_.height;  actorParams_.dy = 0; }      //onGround = true; }
                if (Dy<0)    { actorParams_.y = obj_[i].rect.top + obj_[i].rect.height;   actorParams_.dy = 0; }
                if (Dx>0)    { actorParams_.x = obj_[i].rect.left - actorParams_.width; }
                if (Dx<0)    { actorParams_.x = obj_[i].rect.left + obj_[i].rect.width; }
            }
        }
}

//  =========================================

void GG::setPlayerCoordinateForView(float x, float y, GameContext &context) {
    float tempX = x + actorParams_.width/2;
    float tempY = y + actorParams_.height/2;

    //if (x < 320) tempX = 320;
    //if (y < 240) tempY = 240;//верхнюю сторону
    //if (y > 20) tempY = 20;//нижнюю стороню.для новой карты
    context.View_.setCenter(tempX, tempY);
    context.mWindow->setView(context.View_);
}

//  =========================================