# Samosbor-project

Игра создавалась в качестве выпускного проекта на открытых курсах по С/С++ Технопарка Mail.ru Group & МГТУ им. Н.Э.Баумана в 2018 году.

Состав команды разработки:

*  Бобков Денис (https://gitlab.com/Drenchin)
*  Безруков Егор (https://gitlab.com/Kekor)
*  Кочетков Глеб (https://gitlab.com/RedUse)
*  Смирнова Анита (https://gitlab.com/siberiacalling)